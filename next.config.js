/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['room.markplusinc.com', 'www.marketeers.com', 'secure.gravatar.com', 'i.ytimg.com', 'marketeers.com', 'ads.marketeers.com', 'storage.googleapis.com', 'res.cloudinary.com', 'room.marketeers.com', 'the-marketeers.com', 'imagedelivery.net', 'imgx.gridoto.com', 'bxrepsol.s3.eu-west-1.amazonaws.com', 'aftersales.toyota.astra.co.id','imagedelivery.net','img.freepik.com','localhost'],
  }
}

module.exports = nextConfig
