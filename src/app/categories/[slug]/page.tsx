import Link from "next/link"
import Image from 'next/image'
import parse from 'html-react-parser'
import { getNameCategori } from "@/app/libs/api"
import FindMoreDev from "@/app/components/findmoredev"

async function getData(params: string) {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
          query getCategory {
            category(id: "${params}", idType: SLUG) {
              posts {
                nodes {
                  title
                  content
                  featuredImage {
                    node {
                      altText
                      sourceUrl
                      slug
                    }
                  }
                }
              }
            }
          }`
      })
    })
    const { data } = await res.json()
    return data.category.posts.nodes
  } catch (error) {
    <p>404: Page Not Found</p>
  }
}


export default async function Post({ params }: { params: { slug: string } }) {

  interface Caty {
    seo: {
      metaDesc: string
      title: string
    };
    categories: {
      nodes: {
        name: string
      }[];
    };
    featuredImage: {
      node: {
        altText: string
        sourceUrl: string
        caption: string
      };
    };
    content: string
    title: string
  }

  interface Name {
    name: string
  }

  const data: Caty[] = await getData(params.slug)
  const dt: Name = await getNameCategori(params.slug)
  return (
    <main className="lg:flex lg:flex-col items-center justify-between" >
      <div className="text-white lg:flex bg-no-repeat bg-cover w-full bg-white h-auto">
        <div className="container grid items-start overflow-hidden lg:px-24 lg:w-4/5 p-4">
          <div className="items-center lg:w-full">
            <span className='mt-20 text-6xl mb-4 sm:font-medium text-slate-50 up font-primary fontsectioncap uppercase'>{dt ? dt.name : ""}<span className="text-red-600">.</span></span>
          </div>
        </div>
      </div>
      <div className="w-full h-screen lg:flex lg:flex-col justify-center lg:px-20 bg-white">
        <div className="container lg:flex lg:flex-wrap px-4 gap-4">
          {
            Array.isArray(data) && data.length > 0
              ? data.map((items, i1) => (
                <>
                  <div className="max-w-sm rounded overflow-hidden shadow-xl" key={i1}>
                    <Image
                      src={items.featuredImage ? items.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                      width={500}
                      height={500}
                      alt="Picture of the author"
                    />
                    <div className="px-6 py-4">
                      <div className="font-bold text-xl mb-2">{items ? items.title : ""}.</div>
                      <span className="text-gray-700 text-base">{items ? parse(items.content.substring(0, 50)) : ""}...</span>
                    </div>
                  </div>
                </>
              ))
              : <div className="px-4 text-4xl"><h4>DATA NOT FOUND</h4></div>
          }
        </div>
        {
          Array.isArray(data) && data.length > 0
            ? <div className="flex w-full justify-center items-center mb-32">
              {/* <Link href="#"><button className="px-4 py-2 text-xs uppercase hover:bg-blue-800 duration-300 text-center mt-4 button-primary">find more</button></Link> */}
              < FindMoreDev />
            </div>
            : ""
        }

      </div>
    </main >
  )
}