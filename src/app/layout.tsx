import './globals.css'
import Navigate from './navigate'
import Footer from './footer'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Providers from './providers'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <html lang="en">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover"></meta>
        <body>
          <Providers>
            <Navigate></Navigate>
            {children}
            <Footer></Footer>
          </Providers>
        </body>
      </html>
    </>

  )
}
