import HeadLine from './components/home/headline'
import Capabilities from './components/home/capabilities'
import FeaturedInsights from './components/home/featuredinsights'
import Industries from './components/home/industries'
import TopPage from './components/TopPage'

export default async function Home() {
  
  return (
    <>
      <title>{"Home | MarkPlus.Inc"}</title>
      <TopPage />
      <HeadLine />
      <Capabilities />
      <FeaturedInsights />
      <Industries />
    </>
  )
}

