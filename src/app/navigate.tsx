import Link from "next/link"
import MenuNav from "./components/navigasi/handleNav";
import { getCapabilities, getData, getMenuCategory } from "./components/getCategori";
import Image from 'next/image'
import SearchBox from "./components/search/searchBox";

export default async function Navigate() {

    interface Caty {
        children: {
            nodes: childrens[]
        }
        name: string,
        slug: string,
        categoryId: number,
        link: string,
    }

    interface childrens {
        id: number,
        name: string,
        slug: string
        children: {
            nodes: childs[]
        }
    }

    interface childs {
        name: string
        slug: string
        children: {
            nodes: childs2[]
        }
    }

    interface childs2 {
        name: string
        slug: string
    }

    interface Category {
        name :string
        categoryId : number
        children: {
            nodes: childs[]
        }   
    }

    const data: Caty[] = await getCapabilities()
    const ind: Caty[] = await getMenuCategory("industries")
    const featured: Caty[] = await getMenuCategory("featured insights")
    const about: Caty[] = await getMenuCategory("About Us")

    return (
        <nav className="fixed w-full h-24 shadow-lg bg-white z-10">
            <div className="flex justify-between items-center h-full w-full lg:px-24 px-4">
                <Link href="/" className="lg:h-3/4 h-auto lg:mt-4">
                    <Image
                        src="/../../assets/images/logorev.png"
                        width={130}
                        height={100}
                        alt="Markplus Indonesia"
                        className="w-full h-auto"
                    />
                </Link>
                <div className="hidden sm:flex">
                    <ul className="hidden sm:flex">
                        {
                            ind && ind.map((caty, index) => (
                                <>
                                    <li className='hoverable font-title' key={index}> {/* berikan class hoverable jika terdapat children didalam kategori */}
                                        {
                                            caty.name !== 'Careers'
                                                ?
                                                <>
                                                    <Link href={ "/industries/"} className="py-6 px-4 lg:p-6 mt-2 lg:mt-0 text-slate-800 hover:text-blue-800 mr-4 uppercase">{caty.name ? caty.name : ""}</Link>
                                                    {/* start mega menu */}
                                                    {/* lakukan logika jika terdapat child kategori */}
                                                    <div className="p-4 mega-menu mb-5 sm:mb-0 shadow-xl bg-[#EBEBEB]">
                                                        <div className="px-16 mx-auto w-full flex flex-wrap">
                                                            {caty.children.nodes.map((chil) => (
                                                                <>
                                                                    {chil.name !== "Featured Insights"
                                                                        ? <ul className="px-4 w-full sm:w-1/2 lg:w-1/4 border-gray-600 pb-3 pt-3 lg:pt-3" key={chil.id}>

                                                                            <div className="flex items-center">
                                                                                {/* <Link href={
                                                                                    caty.name === 'Industries'
                                                                                        ? '/industries/' + chil.slug
                                                                                        : caty.name === 'Careers' ? '/careers/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/capabilities/' + chil.slug
                                                                                }
                                                                                    className="text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase" >
                                                                                    {chil.name ? chil.name : ""}</Link> */}

                                                                                <Link href={caty.name === 'Featured Insights' ? '/featured-insight/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/' + chil.slug} className='text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase'>{chil.name ? chil.name : ""}</Link>
                                                                            </div>
                                                                        </ul>
                                                                        : ""}
                                                                </>

                                                            ))}
                                                        </div>
                                                    </div>
                                                    {/* end mega menu */}

                                                </>
                                                : ""
                                        }

                                    </li>
                                </>
                            ))
                        }
                        {
                            data && data.map((caty, index) => (
                                <>
                                    <li className='hoverable font-title' key={index}> {/* berikan class hoverable jika terdapat children didalam kategori */}
                                        {
                                            caty.name !== 'Careers'
                                                ?
                                                <>
                                                    <Link href={caty.name !== 'Featured Insights' ? "#" : "/featuredinsights"} className="py-6 px-4 lg:p-6 mt-2 lg:mt-0 text-slate-800 hover:text-blue-800 mr-4 uppercase">{caty.name ? caty.name : ""}</Link>
                                                    {/* start mega menu */}
                                                    {/* lakukan logika jika terdapat child kategori */}
                                                    <div className="p-4 mega-menu mb-5 sm:mb-0 shadow-xl bg-[#EBEBEB]">
                                                        <div className="px-16 mx-auto w-full flex flex-wrap">
                                                            {caty.children.nodes.map((chil, ic) => (
                                                                <>
                                                                    {
                                                                        <ul className="px-4 w-full lg:w-1/2 border-gray-600 pb-3 pt-3 lg:pt-3" key={ic}>
                                                                            <div className="items-center">
                                                                                <span className='text-dark lg:text-xl font-bold border-b-2 text-slate-800 uppercase'>{chil.name ? chil.name : ""}</span>
                                                                                <div className="border mt-2 border-solid border-slate-400 opacity-50"></div>

                                                                                <ul className="grid grid-cols-2 mt-4 lg:mt-0 gap-4 w-full border-gray-600 pb-3 pt-3 lg:pt-3" >
                                                                                    {
                                                                                        chil.children !== null ? chil.children.nodes.map((dt3, i) => (
                                                                                            <>

                                                                                                <div className="mt-2" key={i}>
                                                                                                    <Link href={'/' + dt3.slug} className='text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase'>{dt3.name}</Link>
                                                                                                </div>

                                                                                            </>
                                                                                        )) : ""
                                                                                    }
                                                                                </ul>

                                                                            </div>
                                                                        </ul>
                                                                    }
                                                                </>

                                                            ))}
                                                        </div>
                                                    </div>
                                                    {/* end mega menu */}

                                                </>
                                                : ""
                                        }

                                    </li>
                                </>
                            ))
                        }
                        {
                            featured && featured.map((caty, index) => (
                                <>
                                    <li className='hoverable font-title' key={index}> {/* berikan class hoverable jika terdapat children didalam kategori */}
                                        {
                                            caty.name !== 'Careers'
                                                ?
                                                <>
                                                    <Link href={caty.name !== 'Featured Insights' ? "#" : "/featuredinsights"} className="py-6 px-4 lg:p-6 mt-2 lg:mt-0 text-slate-800 hover:text-blue-800 mr-4 uppercase">{caty.name ? caty.name : ""}</Link>
                                                    {/* start mega menu */}
                                                    {/* lakukan logika jika terdapat child kategori */}
                                                    <div className="p-4 mega-menu mb-5 sm:mb-0 shadow-xl bg-[#EBEBEB]">
                                                        <div className="px-16 mx-auto w-full flex flex-wrap">
                                                            {caty.children.nodes.map((chil) => (
                                                                <>
                                                                    {chil.name !== "Featured Insights"
                                                                        ? <ul className="px-4 w-full sm:w-1/2 lg:w-1/4 border-gray-600 pb-3 pt-3 lg:pt-3" key={chil.id}>

                                                                            <div className="flex items-center">
                                                                                {/* <Link href={
                                                                                    caty.name === 'Industries'
                                                                                        ? '/industries/' + chil.slug
                                                                                        : caty.name === 'Careers' ? '/careers/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/capabilities/' + chil.slug
                                                                                }
                                                                                    className="text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase" >
                                                                                    {chil.name ? chil.name : ""}</Link> */}

                                                                                <Link href={caty.name === 'Featured Insights' ? '/featured-insight/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/' + chil.slug} className='text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase'>{chil.name ? chil.name : ""}</Link>
                                                                            </div>
                                                                        </ul>
                                                                        : ""}
                                                                </>

                                                            ))}
                                                        </div>
                                                    </div>
                                                    {/* end mega menu */}

                                                </>
                                                : ""
                                        }

                                    </li>
                                </>
                            ))
                        }
                        {
                            about && about.map((caty, index) => (
                                <>
                                    <li className='hoverable font-title' key={index}> {/* berikan class hoverable jika terdapat children didalam kategori */}
                                        {
                                            caty.name !== 'Careers'
                                                ?
                                                <>
                                                    <Link href={caty.name !== 'Featured Insights' ? "#" : "/featuredinsights"} className="py-6 px-4 lg:p-6 mt-2 lg:mt-0 text-slate-800 hover:text-blue-800 mr-4 uppercase">{caty.name ? caty.name : ""}</Link>
                                                    {/* start mega menu */}
                                                    {/* lakukan logika jika terdapat child kategori */}
                                                    <div className="p-4 mega-menu mb-5 sm:mb-0 shadow-xl bg-[#EBEBEB]">
                                                        <div className="px-16 mx-auto w-full flex flex-wrap">
                                                            {caty.children.nodes.map((chil) => (
                                                                <>
                                                                    {chil.name !== "Featured Insights"
                                                                        ? <ul className="px-4 w-full sm:w-1/2 lg:w-1/4 border-gray-600 pb-3 pt-3 lg:pt-3" key={chil.id}>

                                                                            <div className="flex items-center">
                                                                                {/* <Link href={
                                                                                    caty.name === 'Industries'
                                                                                        ? '/industries/' + chil.slug
                                                                                        : caty.name === 'Careers' ? '/careers/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/capabilities/' + chil.slug
                                                                                }
                                                                                    className="text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase" >
                                                                                    {chil.name ? chil.name : ""}</Link> */}

                                                                                <Link href={caty.name === 'Featured Insights' ? '/featured-insight/' + chil.slug : caty.name === 'About Us' ? '/about/' + chil.slug : '/' + chil.slug} className='text-dark border-b-2 text-slate-800 hover:text-blue-900 uppercase'>{chil.name ? chil.name : ""}</Link>
                                                                            </div>
                                                                        </ul>
                                                                        : ""}
                                                                </>

                                                            ))}
                                                        </div>
                                                    </div>
                                                    {/* end mega menu */}

                                                </>
                                                : ""
                                        }

                                    </li>
                                </>
                            ))
                        }


                        <li className="hoverable">
                            {/* <Link href="http://localhost:3000/featuredinsights/" className="py-6 px-4 lg:p-6 mt-2 lg:mt-0 text-blue-900 hover:text-blue-800 mr-4"></Link> */}
                        </li>
                    </ul>

                </div>
                <div className="hidden sm:flex">
                    <SearchBox />
                </div>
                <MenuNav items={data} ind={ind} feat={featured} about={about} /> 
            </div>
        </nav>
    )
}