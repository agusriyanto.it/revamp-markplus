'use client'
import React from 'react'
import { Next13ProgressBar } from 'next13-progressbar'

const providers = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      {children}
      <Next13ProgressBar height="4px" color="#1B1072" options={{ showSpinner: false }} showOnShallow />
    </>
  )
}

export default providers