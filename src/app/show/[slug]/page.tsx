import Breadcrumb from "@/app/components/Breadcrumb"
import { NEXT_PUBLIC_API_ENDPOINT } from "@/app/libs/constant"

async function getData(params: string) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
    cache: 'no-store',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: `
        query getSingelPage {
          posts(where: {categoryName: "${params}"}) {
            nodes {
              contentTypeName
              content(format: RENDERED)
              slug
              title(format: RENDERED)
            }
          }
        }`
    })
  })
  const { data } = await res.json()
  // console.log(data.posts.nodes[0].content)
  return data.posts.nodes
}
async function getCaty(params: string) {
  const caty = await fetch(`${NEXT_PUBLIC_API_ENDPOINT}`, {
    cache: 'no-store',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: `
        query getSingelPage {
          categories(where: {slug: "${params}"}) {
            nodes {
              name
            }
          }
        }`
    })
  })
  const { data } = await caty.json()
  console.log(data.categories.nodes)
  return data.categories.nodes
}


export default async function Post({ params }: { params: { slug: string, title: string, content: string }  }) {
  interface gCaty{
    name : string,
    slug : string
  }
  const data = await getData(params.slug)
  const caty:gCaty = await getCaty(params.slug)
  return (
    <main className="container flex-col sm:w-full sm:h-full mx-auto" >
      <div className="flex place-items-end p-4  pt-36 px-4 gap-4">
        <Breadcrumb pageName={caty.name} />
      </div>
      <div className="lg:flex  place-items-center">
        <div className="w-full object-center p-4">
          {
            Array.isArray(data) && data.length > 0 
            ? 
            <>
            <div className="items-center text-4xl font-bold underline mb-5">{data[0].title} </div>
            <div dangerouslySetInnerHTML={{ __html: data[0].content }} className="items-center"></div></>
            : <h4 className="align-middle">404 - Page Not Found</h4>
            
          }
        </div>
      </div>
    </main>
  )
}



