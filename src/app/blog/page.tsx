async function getData(params: string) {
    const caty = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            query: `
        query getSingelPage {
            posts(where: {name: "${params}"}) {
                nodes {
                  slug
                  content(format: RENDERED)
                  title(format: RENDERED)
                }
              }
        }`
        })
    })
    const { data } = await caty.json()
    return data.posts.nodes
}

export default async function Post({ params }: { params: { slug: string, title: string, content: string,name:string } }) {
    const data = await getData(params.slug)
    return (
        <main className="container flex-col sm:w-full sm:h-full mx-auto" >
            <div className="flex place-items-end p-4  pt-36 px-4 gap-4">
                {/* <Breadcrumb pageName={data[0].title} /> */}
            </div>
            <div className="container w-full h-full ">
               
                        <>
                            <div className="text-4xl font-bold underline mb-5 text-center">Blog </div>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nesciunt eveniet doloremque inventore iure libero nulla nostrum deserunt! Id vero omnis quas atque ab sint voluptates placeat sapiente odio! At enim laborum, magni similique et inventore excepturi totam sequi nisi quasi eligendi beatae quia sapiente? Fugiat magnam suscipit blanditiis ea sapiente maxime natus dolor, itaque officia laboriosam asperiores beatae vitae facere. Sunt quae placeat dicta non dolor ipsa voluptate at amet? Iure, nesciunt doloribus cupiditate natus quia ad exercitationem vitae nihil minima tempore quaerat maiores suscipit laboriosam tempora dolor rerum iste veritatis fuga dolorem! Similique omnis tenetur minima officia pariatur nihil?</p></>
                        
            </div>
        </main>
    )
}