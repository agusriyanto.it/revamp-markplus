import { getPostslugs, getSinglePg } from '@/app/libs/api'
import parse from 'html-react-parser'

interface Caty {
    categories: {
        nodes: {
            name: string
            slug: string
        }
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
            caption: string
        };
    };
    content: string
    slug: string
    title: string
}
export default async function Preview({ params }: { params: { slug: string } }) {
    const data: Caty = await getSinglePg(params.slug)
    return (
        <main className="flex-col sm:w-full sm:h-full mx-auto" >
            <>
                {
                    data && data
                        ? <>
                            <div className="text-white lg:flex bg-no-repeat bg-cover w-full lg:h-screen h-72 justify-center" style={{ backgroundImage: data.featuredImage !== null ? `url(${data.featuredImage.node.sourceUrl})` : 'url("/../../assets/images/img-default.jpg")' }} >
                                <div className='lg:py-28 ' >
                                    <div className="overflow-hidden lg:px-24 lg:w-2/4 p-6 items-start">
                                        <div className="items-center">
                                            {/* <p className='mt-4 mb-4 sm:font-medium text-slate-50 text-sm'>{data.featuredImage.node.altText}</p>
                                    <h2 className='mb-20 text-6xl font-bold font-banner uppercase'>{data.featuredImage.node.caption}</h2> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full flex flex-col justify-center items-center lg:px-20 bg-white">
                                <div className="container w-2/3 lg:flex  lg:flex-wrap mt-24 mb-24">
                                    <div className="text-4xl font-bold underline mb-5 text-center font-body ">{data.title ? data.title : ""} </div>
                                    <div className="items-center font-body ">{data.content ? parse(data.content) : "Data Not Found"}</div>
                                </div>
                            </div>
                        </>
                        : <div className="w-full h-screen flex flex-col justify-center items-center lg:px-20 lg:py-24 bg-white">
                            <p className="text-9xl">Data Not Found</p>
                        </div>
                }
            </>
        </main>
    )
}