import parse, { domToReact } from 'html-react-parser';
import Image from 'next/image';
import Link from 'next/link';

export default function parseHtml(html) {

  const options = {
    replace: ({ name, attribs, children }) => {
      const isInternalLink = name === "a" && attribs["data-internal-link"] === true;
      const image = name === "img";
      const paragraf = name === "p";
      const headingSecond = name === "h2";
      const headingThird = name === "h3";
      const span = name === "span";
      const externalLink = name === "a";
      const UnorderedList = name === "ul";
      const ListItem = name === "li";
      const OrderedList = name === "ol";
      const white = name === "&nbsp;";


      if (!attribs) {
        return;
      }


      if (externalLink) {
        if (attribs.href.startsWith('https://room.markplusinc.com/') === true) {
          let newUrl = attribs.href.replace(/(https:|)(^|\/\/)(.*?\/)/g, 'https://staging.markplusinc.com/');
          return (
            <a href={newUrl} style={{ color: '#0ea5e9', fontStyle: 'italic', fontWeight: '600' }} target='_blank' rel="noopener noreferrer">{domToReact(children, options)}</a>
          );
        } else if (attribs.href.startsWith('https://staging.markplusinc.com/') === true) {
          return (
            <a href={attribs.href} style={{ color: '#0ea5e9', fontStyle: 'italic', fontWeight: '600' }} rel="noopener noreferrer">{domToReact(children, options)}</a>
          );

        } else
          return (
            <a href={attribs.href} style={{ color: '#0ea5e9', fontStyle: 'italic', fontWeight: '600' }} target='_blank' rel="noopener noreferrer">{domToReact(children, options)}</a>
          );
      }

      if (isInternalLink) {

        return (
          <a href={attribs.href} style={{ color: '#0ea5e9', fontStyle: 'italic', fontWeight: '600' }} rel="noopener noreferrer">{domToReact(children, options)}</a>
        );
      }

      if (attribs.class === "wp-caption-text" || name === "figcaption") {
        return (
          <figcaption className='sm:mb-4 mb-0'>
            <span className='font-light text-gray-400 sm:text-base text-sm'>{domToReact(children, options)}</span>
          </figcaption>);
      }

      if (headingSecond) {
        return <h2 className='font-container-content-heading-seconds font-container-margin-heading-seconds'>{domToReact(children, options)}</h2>;
      }

      if (headingThird) {
        return <h3 className='font-container-content-heading-seconds font-container-margin-heading-seconds'>{domToReact(children, options)}</h3>;
      }

      if (paragraf) {
        return <p className='font-container-content font-container-margin'>{domToReact(children, options)}</p>;
      }

      if (span) {
        return <span className='font-container-content'>{domToReact(children, options)}</span>;
      }

      if (white) {
        return <h4 className='font-container-content'>{domToReact(children, options)}</h4>;
      }
      
      if (UnorderedList) {
        return <ul className='font-container-content font-container-ul'>{domToReact(children, options)}</ul>;
      }
      
      if (ListItem) {
        return <li className='font-container-content'>{domToReact(children, options)}</li>;
      }
      if (OrderedList) {
        return <ol className='font-container-content font-container-ol li'>{domToReact(children, options)}</ol>;
      }

      if (attribs.class === "wp-block-image") {
        return (
          <div className='relative'>
            {domToReact(children, options)}
          </div>);
      }

      if (attribs.class === "wp-caption aligncenter" || attribs.class === "wp-caption alignnone") {
        return (
          <figure className='relative py-2'>
            {domToReact(children, options)}
          </figure>);
      }


      if (image) {
       return (<>
        <Image
          src={attribs.src}
          alt={attribs.alt}
          width={200}
          height={112.5}
          quality={90}
          // blurDataURL={'/assets/image/blur.png'}
          // placeholder="blur"
          className='w-full h-auto mt-14'
          sizes="(max-width: 768px) 100vw,
        (max-width: 1200px) 50vw,
        33vw"
        />
      </>)
      }
    }

  };

  return parse(html, options);

};