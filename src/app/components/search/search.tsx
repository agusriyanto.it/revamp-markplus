"use client"

import { useState } from "react"
import Swal from "sweetalert2"

const Search = () => {
    const [query, setQuery] = useState('')

    const handleSubmit = () => {
        alert(query)
    }

    return (
        <>
            <div className="flex w-full mt-10 h-24 px-20 bg-white items-center justify-between">
                <div className="mt-2 w-full flex gap-4">
                    <form onSubmit={handleSubmit} className="w-full">
                        <div className="flex items-center w-full my-12">
                            <div className="flex items-center w-full relative">
                                <input
                                    type="text"
                                    className="border-none w-full outline-none text-2xl h-20 pr-4 pl-14 py-4 bg-gray-100 focus:outline-none font-titles focus:border-none transition-all" placeholder="Enter keywords here"
                                    name="q"
                                    onChange={(e) => setQuery(e.target.value)}
                                     />
                                <div className="border-none bg-transparent absolute top-6 left-4">
                                    <svg className="icon icon-search" focusable="false" viewBox="0 0 32 32" width="32" height="32" xmlns="http://www.w3.org/2000/svg" data-testid="iconSearch">
                                        <title>Search</title>
                                        <path d="M14.5 10a4.5 4.5 0 1 0 0 9 4.5 4.5 0 0 0 0-9zm5.249 8.335l4.458 4.458-1.414 1.414-4.458-4.458a6.5 6.5 0 1 1 1.414-1.414z" fill-rule="nonzero"></path>
                                    </svg>
                                </div>
                            </div>
                            <button type="submit" className="bg-[#1B1072] px-4 py-4 h-20 text-white uppercase text-titles text-2xl hover:bg-blue-900 transition-all">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default Search