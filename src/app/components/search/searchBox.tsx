"use client"

import Link from "next/link"
import { useRouter } from "next/navigation";
import { useState } from "react"
import { AiOutlineClose, AiOutlineSearch } from "react-icons/ai"

interface Caty {
    children: {
        nodes: childrens[];
    };
    name: string,
    slug: string,
    categoryId: number,
    link: string,
}

interface childrens {
    id: number,
    name: string,
    slug: string
}

interface MenuProps {
    items: Caty[];
}

const SearchBox = () => {
    const [formOpen, setFormOpen] = useState(false)
    const [keyword, setKeyword] = useState('')
    const router = useRouter()

    const handleShow = () => {
        setFormOpen(!formOpen)
    }
   
    const onSubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        router.push(`/search/${keyword}`)
        handleShow()
    }
    return (
        <>
            <div onClick={handleShow} className="cursor-pointer">
                <AiOutlineSearch className="mt-1 text-3xl" />
            </div>
            <div className={
                formOpen
                    ? "fixed left-0 top-0 w-full z-10 bg-slate-800 bg-opacity-40 h-screen ease-in duration-500"
                    : "fixed left-[-100%] top-0 h-28 ease-in duration-500"
            }>
                <div className="flex w-full h-28 lg:px-24 px-2 bg-white items-center justify-between">
                    <form className="mt-2 w-full flex gap-4 z-0">
                        <div className="w-full flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600">
                            <input
                                type="text"
                                name="search"
                                id="first-name"
                                value={keyword}
                                onChange={(e) => setKeyword(e.target.value)}
                                placeholder="Type to search..."
                                className="flex-1 lg:text-4xl text-xl font-body py-2 border-b-2 border-gray-400 focus:border-[#1B1072] 
                                text-gray-600 placeholder-gray-400
                                outline-none"
                            />
                        </div>
                        <button type="submit" onClick={onSubmit}><AiOutlineSearch className="text-2xl"/></button>
                    </form>
                    <div onClick={handleShow} className="flex cursor-pointer items-center p-4">
                        <AiOutlineClose className="text-2xl"/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SearchBox