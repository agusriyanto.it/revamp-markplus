'use client'

import Link from 'next/link';
import React from 'react';
import Slider from 'react-slick';
import Image from 'next/image'

interface Caty {
    categories: {
      nodes: categori[]
    }
    featuredImage: {
      node: {
        altText: string
        sourceUrl: string
        caption: string
      };
    };
    slug: string
    title: string
    name: string
  }
  interface categori {
    parent: {
      node: {
        name: string
      }
    }
  }

interface MenuProps {
    item: Caty[];
}

var setting = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 5000,
    pauseOnHover: true,
}
const BannerImgSlide: React.FC<MenuProps> = ({ item }) => {
    return (
        <>
            <Slider {...setting}>
            {
            item && item.map((dt, index) => (
              <>
                <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }} key={index}>
                  <div className='w-full relative lg:h-[420px] min-h-[21rem]'>
                    <Image
                        src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : '/../../assets/images/featured1.jpg'}
                        alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                        fill
                        className='w-full h-auto brightness-75'
                        objectFit="cover"
                        sizes="100vw"
                        placeholder="blur"
                        blurDataURL={`/../../assets/images/bpou.jpg`}
                    />
                    <div className="absolute">
                      <div className="lg:py-24 lg:w-4/5 w-full lg:px-24 p-4 text-white" >
                        
                            {
                                dt.categories.nodes.map((dt2,i) => (
                                    <h2 className='text-xs font-title uppercase lg:mt-36 mt-40' key={i}>{dt2.parent.node.name}</h2>
                                ))
                            }
                        <span className='mt-10 lg:text-2xl text-[18px] sm:font-medium font-title text-slate-50 uppercase'>{dt ? dt.title : ""}</span>
                        <div>
                          <Link href={dt.slug} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            ))
          }
            </Slider>
        </>
    )
}

export default BannerImgSlide