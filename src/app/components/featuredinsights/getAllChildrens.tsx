import Link from 'next/link'
import { AiFillCaretRight } from 'react-icons/ai'
import { getHomeFi, getPostBySlugFi } from '@/app/libs/api'
import Image from 'next/image'

interface Caty {
    name: string
    slug: string
    posts: {
        edges: post[]
    }
}

interface post {
    name : string
    node: {
        title: string
        slug: string
        featuredImage: {
            node: {
                altText: string
                sourceUrl: string
            }
        }
        seo: {
            metaDesc: String
        }
    }
}

const GetAllChildrens = async () => {

    const Ideas: Caty[] = await getHomeFi('ideas')
    const Insight: post[] = await getPostBySlugFi('insights')
    console.log(Insight)
    const Inpres: post[] = await getPostBySlugFi('in-the-press')
    return (
        <section className="w-full h-auto flex flex-col">
            <div className="w-full lg:h-16 h-14 lg:mt-10 text-dark text-white bg-blue-950 " >
                <Link href='/featured-insight/ideas' className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
                    <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase lg:text-2xl text-xl'>IDEAS</p>
                </Link>
            </div>
            <div className="h-auto lg:w-full lg:grid lg:grid-cols-4 lg:px-24 bg-white gap-2 p-4">
                {
                    Array.isArray(Ideas) && Ideas.length > 0
                        ? Ideas.map((dt, i) => (
                            dt.posts.edges.map((dtp, ip) => (
                                <>
                                    <div className="w-full h-auto justify-between lg:flex lg:mt-14 mt-10" key={i}>
                                        <Link href={dtp.node.slug} className="lg:flex lg:flex-col text-gray-700 bg-white  bg-clip-border w-96">
                                            <div className="w-full h-auto" key={ip} >
                                                <Image
                                                    src={dtp.node.featuredImage !== null ? dtp.node.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                    alt={dtp.node.featuredImage !== null ? dtp.node.featuredImage.node.altText : "Picture of the author"}
                                                    width={200}
                                                    height={112.5}
                                                    quality={90}
                                                    className='w-full h-[220px]'
                                                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                />
                                            </div>
                                            <div className="w-full h-auto ">
                                                <div className="lg:text-md text-[14px] text-slate-800  lg:mt-4 mt-2 font-title uppercase">
                                                    {dt.name}<span className='text-rose-700'>.</span>
                                                </div>
                                                <div className="lg:text-2xl text-xl hover:underline font-titlea text-[#1B1072] uppercase">{dtp ? dtp.node.title : ""}</div>
                                                <h2 className='lg:text-xl text-[14px] font-body text-slate-800 lg:mt-4 mt-2'>{dtp ? dtp.node.seo.metaDesc.substring(0, 110)+ " ..." : ""}</h2>
                                            </div>
                                        </Link>
                                    </div>
                                </>
                            ))
                        ))
                        : ""
                }

            </div>

            <div className="w-full lg:h-16 h-14 mt-10 text-dark text-white bg-blue-950 " >
                <Link href='/featured-insight/insights' className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
                    <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase lg:text-2xl text-xl'>INSIGHTS</p>
                </Link>
            </div>
            <div className="h-auto lg:w-full lg:grid lg:grid-cols-4 lg:px-24 bg-white gap-2 p-4">
                {
                    Array.isArray(Insight) && Insight.length > 0
                        ? Insight.map((dt, i) => (
                                <>
                                    <div className="w-full h-auto justify-between lg:flex mt-5" key={i}>
                                        <Link href={dt.node.slug} className="lg:flex lg:flex-col text-gray-700 bg-white bg-clip-border w-96">
                                            <div className="w-full h-auto" >
                                                <Image
                                                    src={dt.node.featuredImage !== null ? dt.node.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                    alt={dt.node.featuredImage !== null ? dt.node.featuredImage.node.altText : "Picture of the author"}
                                                    width={200}
                                                    height={112.5}
                                                    quality={90}
                                                    className='w-full h-[220px]'
                                                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                />
                                            </div>
                                            <div className="w-full h-auto ">
                                                <div className="lg:text-md text-[14px] text-slate-800  lg:mt-4 mt-2 font-title uppercase">
                                                    {dt ? dt.name :""}<span className='text-rose-700'>.</span>
                                                </div>
                                                <div className="lg:text-2xl text-xl hover:underline font-titlea text-[#1B1072] uppercase">{dt ? dt.node.title : ""}</div>
                                                <h2 className='lg:text-xl text-[14px] font-body text-slate-800 lg:mt-4 mt-2'>{dt ? dt.node.seo.metaDesc.substring(0, 110)+ " ..." : ""}</h2>
                                            </div>
                                        </Link>
                                    </div>
                                </>
                            ))
                        : ""
                }
            </div>

            <div className="w-full lg:h-16 h-14 mt-10 text-dark text-white bg-blue-950 " >
                <Link href='/featured-insight/in-the-press' className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
                    <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase lg:text-2xl text-xl'>IN THE PRESS</p>
                </Link>
            </div>
            <div className="h-auto lg:w-full lg:grid lg:grid-cols-4 lg:px-24 bg-white gap-2 p-4">
            {
                    Array.isArray(Inpres) && Inpres.length > 0
                        ? Inpres.map((dt, i) => (
                                <>
                                    <div className="w-full h-auto justify-between lg:flex mt-5" key={i}>
                                        <Link href={dt.node.slug} className="lg:flex lg:flex-col text-gray-700 bg-white bg-clip-border w-96">
                                            <div className="w-full h-auto" >
                                                <Image
                                                    src={dt.node.featuredImage !== null ? dt.node.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                    alt={dt.node.featuredImage !== null ? dt.node.featuredImage.node.altText : "Picture of the author"}
                                                    width={200}
                                                    height={112.5}
                                                    quality={90}
                                                    className='w-full h-[220px]'
                                                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                />
                                            </div>
                                            <div className="w-full h-auto ">
                                                <div className="lg:text-md text-[14px] text-slate-800  lg:mt-4 mt-2 font-title uppercase">
                                                    {dt ? dt.name :""}<span className='text-rose-700'>.</span>
                                                </div>
                                                <div className="lg:text-2xl text-xl hover:underline font-titlea text-[#1B1072] uppercase">{dt ? dt.node.title : ""}</div>
                                                <h2 className='lg:text-xl text-[14px] font-body text-slate-800 lg:mt-4 mt-2'>{dt ? dt.node.seo.metaDesc.substring(0, 110)+ " ..."  : ""}</h2>
                                            </div>
                                        </Link>
                                    </div>
                                </>
                            ))
                        : ""
                }
            </div>
            <div className="w-full h-auto lg:flex lg:flex-col p-4 lg:px-20 bg-white">
                {/* {
                    data ? data.map((dt, index) => (
                        <>
                            <div className='w-full h-auto py-14 ' key={index}>
                                <span className='text-4xl font-bold text-[#1B1072] font-titlea uppercase'>{data ? dt.name : ""}</span>
                                <p className='text-xl font-body mt-5'>{data ? parseHtml(dt.description) : ""}</p>
                            </div>
                            <div className="lg:w-full h-auto flex flex-col gap-4 mb-10">
                            {
                                dt && dt.children.nodes.map((dt2, index2) => (
                                    <>
                                        <div className="w-full h-auto hover:shadow-lg hover:border-2 justify-between lg:flex" key={index2}>
                                            <div className="w-full h-auto">
                                                <Image
                                                    src={"/../../assets/images/featured3.jpg"}
                                                    alt="Picture of the author"
                                                    width={200}
                                                    height={112.5}
                                                    quality={90}
                                                    className='w-full h-auto'
                                                    sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                />
                                            </div>
                                            <div className="w-full h-auto p-8 gap-4">
                                                <div className="w-full h-auto ">
                                                    <div className="text-4xl font-bold text-[#1B1072] mb-5 font-titlea">{dt2 ? dt2.name : "" }</div>
                                                    <div className="text-xl font-body text-slate-800">{dt2 ? dt2.description : "" }</div>
                                                    <div className="text-2xl mt-4">
                                                        <Link href={"../"} className="flex hover:translate-x-4 transition ease-in-out delay-500 duration-300 hover:text-blue-700">
                                                            <div className="text-[#1B1072] leading-none">Read More </div>
                                                            <AiOutlineArrowRight className="ml-2 " />
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                ))
                            }
                            </div>
                            <hr className="my-4 border-1 border-gray-300" />
                        </>
                    ))
                        : ""
                } */}


            </div>
        </section >
    )
}

export default GetAllChildrens
