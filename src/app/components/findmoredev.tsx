"use client"

import Swal from "sweetalert2"

const FindMoreDev = () => {
    const handle = () => {
        Swal.fire({
            title: "Ups...!",
            text: "Sorry, Under development.",
            icon: "info"
          });
    }

    return (
        <>
           <button className="px-4 py-2 text-xs uppercase hover:bg-blue-800 duration-300 text-center mt-4 button-primary" onClick={handle}>find more</button>
        </>

    )
}

export default FindMoreDev