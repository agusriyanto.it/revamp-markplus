'use client'

import Link from 'next/link'
import React, { useState } from 'react'
import Image from 'next/image'

interface MenuProps {
    items: {
        nodes: Caty[]
        pageInfo: {
            endCursor: String
            hasNextPage: Boolean
            hasPreviousPage: Boolean
        }
    }
    keyword: string
}

interface Caty {
    title: string
    slug: string
    date: Date
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
        }
    }
    seo: {
        metaDesc: string
    }
    categories: {
        nodes: categ[]
    }
}

interface categ {
    parent: {
        node: {
            name: string
        }
    }
}

const Card: React.FC<MenuProps> = ({ items: initialItems, keyword }) => {
    const [items, setItems] = useState(initialItems)
    const [isLoading, setIsLoading] = useState(false)
    const getSearch = async (params: String, after: String) => {
        try {
            setIsLoading(true)
            const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
                cache: 'no-store',
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    query: `
              query getSearchPosts($searchQuery: String!,$first: Int!, $after: String) {
                posts(
                  where: {search: $searchQuery,status: PUBLISH, orderby: {field: DATE, order: DESC}, categoryNotIn: "62"}
                  first: $first, after: $after
                ) {
                  nodes {
                    title
                    slug
                    date
                    featuredImage {
                      node {
                        altText
                        sourceUrl
                      }
                    }
                    seo {
                      metaDesc
                    }
                    categories {
                      nodes {
                        parent {
                          node {
                            name
                          }
                        }
                      }
                    }
                  }
                  pageInfo {
                    endCursor
                    hasNextPage
                    hasPreviousPage
                  }
                }
              }`,
                    variables: {
                        searchQuery: params,
                        first: 6,
                        after: after
                    }
                })
            })
            if (!resp.ok) {
                throw new Error('Failed to fetch data')
            }
            const { data } = await resp.json()
            const newData = {
                ...data.posts,
                nodes: [...items.nodes, ...data.posts.nodes]
            }
            setItems(newData)
        } catch (error) {
            console.error('Error fetching data')
        } finally {
            setIsLoading(false); 
        }
    }

    const handleLoadMore = (e: React.FormEvent) => {
        e.preventDefault()
        getSearch(keyword, items.pageInfo.endCursor)
    }
    return (
        <>
            <div className="h-auto lg:w-full lg:grid lg:px-20 lg:grid-cols-3 bg-white lg:p-4 gap-4 lg:mb-24 mb-10">
                {items ? items.nodes.map((dt, i) => (
                    dt.seo.metaDesc 
                    ?
                    <>
                        <div className="w-full h-auto justify-between lg:flex lg:mt-14 mt-10">
                            <Link href={'/' + dt.slug} className="relative lg:flex lg:flex-col mt-6 text-gray-700 bg-white  hover:shadow-lg bg-clip-border w-96" key={i} >
                                <div className="w-full h-auto">
                                    <Image
                                        src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/featured3.jpg"}
                                        alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                        width={200}
                                        height={112.5}
                                        quality={90}
                                        className='w-full h-[220px]'
                                        sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                    />
                                </div>
                                <div className="w-full h-auto gap-4">
                                    <div className="w-full h-auto ">
                                        <div className="lg:text-md text-[14px] text-slate-800  lg:mt-4 mt-2 font-title uppercase">{dt ? dt.categories.nodes.map((dt2, i2) => (<span key={i2}>{dt2 ? dt2.parent.node.name : ""}</span>)) : ""}</div>
                                        <div className="lg:text-2xl text-xl font-titlea text-[#1B1072] uppercase">{dt ? dt.title : ""}</div>
                                        <h2 className='lg:text-xl text-[14px] font-body text-slate-800 lg:mt-4 mt-2'>{dt.seo.metaDesc ? dt.seo.metaDesc.substring(0, 110)+ " ..." : ""}</h2>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </>
                    : ""
                    
                )) : ""}
            </div>
            {
                items.pageInfo.hasNextPage && (    
                    isLoading 
                    ? <div className='grid justify-items-center'>
                        <div className="circle"></div>
                    </div>  
                    :            
                    <form method="post" className='grid justify-items-center' onSubmit={handleLoadMore}>
                        <div>
                        <button type="submit" disabled={isLoading} className='bg-[#1B1072] text-white p-4 text-md uppercase font-title hover:bg-blue-900 hover:scale-100 duration-100' > Find More</button></div>
                    </form>            
                )
            }
        </>
    )
}

export default Card