import Link from 'next/link'
import { getHeadlines } from '@/app/libs/api'
import ImageHeadline from './imgheadline'
import Image from 'next/image'
import { AiFillCaretUp } from 'react-icons/ai'
import ScrollButton from '../buttontop'

export default async function HeadLine() {

  const data = await getHeadlines()
  return (
    <section className="text-white sm:w-full mx-auto" >
      {
        // data ? data.length > 0 ? <ImageHeadline items={data} /> :
        <>
          <div className="relative top-16 w-full h-screen" >
            <div className='w-full relative h-screen bg-white'></div>
          </div>
          <div className='absolute w-full top-0 p-4 lg:px-24'>
            <div className="grid items-center text-center lg:w-full h-screen">
              <div className=' mt-4 text-2xl lg:w-full  md:text-4xl'>
                <h2 className='text-7xl uppercase fontsectioncap text-[#1B1072]'>Marketing <br />is Markplus<span className='text-red-600'>.</span> </h2>
                <p className='text-black lg:text-2xl text-xl lg:w-full font-body mt-8'>Our firm is synonymous with marketing. <br />Industry players see us as the go-to firm for everything marketing, from insight to strategy to execution. </p>
              </div>
            </div>
          </div>

          {/* <div className="relative top-16 w-full lg:h-screen" >
              <div className='w-full relative lg:h-screen'>
                <Image
                  src={'/../../assets/images/home.jpg'}
                  alt={"Picture of the author"}
                  width={200}
                  height={112.5}
                  quality={100}
                  priority
                  className='w-full h-screen'
                  sizes="100vw"
                  style={{
                    objectFit: 'cover',
                  }}
                />

              </div>
            </div>
            <div className='absolute w-full lg:top-0 top-28 p-4 lg:px-24'>
              <div className="grid items-end lg:w-2/4 h-screen">
                <div className=' mt-4 text-2xl lg:w-full lg:mb-32 mb-80 md:text-4xl font-bold font-banner uppercase'>
                  <h4 className='flex text-slate-50 text-xl uppercase font-bold font-title'>Markplus <AiFillCaretUp className='place-items-end text-red-500 text-sm justify-end mt-2' />inc</h4>
                  <p className='mt-4 lg:text-7xl text-4xl'>Kami adalah pelopor dalam dunia pemasaran yang telah menjadi bagian dalam menciptakan terobosan besar di banyak perusahaan
</p>
                </div>
              </div>
            </div> */}
        </>
        // : ""
      }
      <div className='grid place-items-end z-0' >
        <div className='fixed right-4 lg:right-20 bottom-10 lg:bottom-16'>
          <ScrollButton />
        </div>
      </div>
    </section>
  )
}

