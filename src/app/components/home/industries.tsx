import { getIndustriesAsc, getIndustriesDesc } from '@/app/libs/api';
import { AiFillCaretRight } from 'react-icons/ai';
import BannerImgSlide from '../bannerSlide/bannerImgSlide';

interface Capt {
    categories: {
        nodes: categori[]
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
            caption: string
        };
    };
    slug: string
    title: string
    name: string
}
interface categori {
    parent: {
        node: {
            name: string
        }
    }
}

export default async function Industries() {

    const data: Capt[] = await getIndustriesDesc()
    const dataasc: Capt[] = await getIndustriesAsc()
    console.log
    return (
        <>
            <div className="lg:flex lg:py-28 py-10 lg:flex-col bg-white">
                <div className='w-full text-center'>
                    <div className="lg:flex p-6 lg:py-12 flex-col justify-center items-center">
                        <p className="text-8xl uppercase text-center fontsectioncap text-[#1B1072]">we <br /> understand <br />indonesia<span className='text-red-600'>.</span></p>
                        <p className='text-black lg:text-2xl text-base lg:w-4/5 font-body lg:mt-8 mt-4 text-center'>Nobody knows the Indonesian market better than we do.<br /> Consumer research, market intelligence, and expert network are at the heart of what we do. </p>
                    </div>
                </div>
                <div className="w-full h-16 text-dark text-white bg-blue-950 lg:mt-28 mt-10">
                    <div className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
                        <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase '>Industries</p>
                    </div>
                </div>
                <div className='lg:grid lg:grid-cols-2'>
                    <BannerImgSlide item={data} />
                    <BannerImgSlide item={dataasc} />
                </div>
            </div>
        </>

    )
}
