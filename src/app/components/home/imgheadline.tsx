'use client'

import Link from 'next/link';
import React from 'react';
import Slider from 'react-slick';
import Image from 'next/image'
import { AiFillCaretUp } from 'react-icons/ai';
import ScrollButton from '../buttontop';

interface Caty {
    seo: {
        metaDesc: string
        title: string
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
            caption: string
        };
    };
    content: string
    slug: string
    title: string
    categories: {
        nodes: categori[]
    }
}

interface categori {
    parent: {
        node: {
            name: string
        }
    }
}

interface MenuProps {
    items: Caty[];
}

var settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 5000,
    pauseOnHover: true
}

const ImageHeadline: React.FC<MenuProps> = ({ items }) => {
    return (
        <Slider {...settings}>
            {
                items.length > 0 ? items.map((dt, index) => (
                    <>
                        <div className="relative w-full lg:h-auto" key={index}>
                             <div className='w-full relative lg:h-screen min-h-[21rem]'>
                                <Image
                                    src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : '/../../assets/images/home.jpg'}
                                    alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                    fill
                                    className='w-full h-auto brightness-75'
                                    objectFit="cover"
                                    sizes="100vw"
                                />
                            </div>
                        </div>
                        <div className='absolute w-2/4 lg:top-0 top-16 p-4 lg:px-24'>
                            <div className="grid items-end lg:w-2/4 lg:h-screen">
                                <div className='lg:text-7xl lg:mt-4 mt-16 text-2xl lg:w-full lg:mb-32 uppercase'>                                    
                                        {dt.categories.nodes.map((dt2, i) => (
                                            <h4 className='flex text-slate-50 lg:text-xl text-base font-title uppercase' key={i}>{dt2.parent.node.name}</h4>
                                        ))}
                                    <h4 className='lg:w-2/4 w-2/6 font-banner text-white'>{dt.title ? dt.title : ""}</h4>
                                    <div>
                                        <Link href={"/" + `${dt.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-2'> Read Now</Link>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </>
                ))
                    : ""
            }
        </Slider>
    );
};

export default ImageHeadline;
