import Link from 'next/link'
import { AiFillCaretRight, AiFillCaretUp } from 'react-icons/ai'
import { getCapabilitiesHome } from '../getCategori'
import Image from 'next/image'
import { getHeadlines } from '@/app/libs/api'
import ImageHeadline from './imgheadline'
import BannerImgSlide from '../bannerSlide/bannerImgSlide'

export default async function Capabilities() {
  interface Capt {
    categories: {
      nodes: categori[]
    }
    featuredImage: {
      node: {
        altText: string
        sourceUrl: string
        caption: string
      };
    };
    slug: string
    title: string
    name: string
  }
  interface categori {
    parent: {
      node: {
        name: string
      }
    }
  }

  const data: Capt[] = await getCapabilitiesHome(145)
  const res: Capt[] = await getCapabilitiesHome(146)
  const headline = await getHeadlines()
  return (
    <>
      {headline ? headline.length > 1 ? <ImageHeadline items={headline} /> :
        <div className="w-full text-dark mt-16 bg-white">
          <div className="relative w-full h-screen" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }} >
            <div className='w-full relative h-screen'>
              <Image
                src={'/../../assets/images/home.jpg'}
                alt={"Picture of the author"}
                fill
                priority
                className='w-full h-auto brightness-75'
                objectFit="cover"
                sizes="100vw"
              />
              <div className='absolute w-full lg:top-0 top-28 p-4 lg:px-24'>
                <div className="grid items-end lg:w-2/4 text-white h-screen">
                  <div className=' mt-4 text-2xl lg:w-full lg:mb-72 mb-80 md:text-4xl font-bold font-banner uppercase'>
                    <h4 className='flex text-slate-50 text-xl uppercase font-bold font-title'>Markplus <AiFillCaretUp className='place-items-end text-red-500 text-sm justify-end mt-2' />inc</h4>
                    <p className='mt-4 lg:text-7xl text-4xl'>The Leading Marketing Consulting in Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        : ""}

      {/* <ImageHeadline items={headline} /> */}
      <div className="flex flex-col">
        <div className="w-full h-16 text-dark text-white bg-blue-950 ">
          <div className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
            <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase '>Capabilities</p>
          </div>
        </div>
        <div className='lg:grid lg:grid-cols-2'>
          <BannerImgSlide item={data} />
          <BannerImgSlide item={res} />
        </div>
      </div >
    </>

  )
}
