import { getLatesPostByName } from '@/app/libs/api'
import Link from 'next/link'
import { AiFillCaretRight } from 'react-icons/ai'
import Image from 'next/image'
import parse from 'html-react-parser'

interface Fi {
    slug: string
    title: string
    categories: {
        nodes: categori[]
    }
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    seo: {
        metaDesc: string
    }
}

interface categori {
    name: string
    slug: string
}

export default async function Headerthree() {
    const data: Fi[] = await getLatesPostByName(47)
    const dataip: Fi[] = await getLatesPostByName(49)
    const datains: Fi[] = await getLatesPostByName(48)
    return (
        <section className="flex flex-col items-center justify-between">
            <div className="w-full lg:flex items-center justify-between lg:py-48 py-12 text-dark bg-white">
                <div className='w-full lg:-translate-x-80 '>
                    <Image
                        src={'/../../assets/images/unlockgrow.jpg'}
                        alt={'wow 1'}
                        width={200}
                        height={112.5}
                        quality={90}
                        className='w-full'
                        sizes='(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw'
                    />
                </div>
                <div className='w-full items-center justify-between text-dark mt-10 bg-white'>
                    <div className="lg:justify-items-end lg:p-6 p-4 lg:text-right lg:px-24">
                        <h2 className='text-8xl font-bold uppercase fontsectioncap text-[#1B1072]'>growth<br />unlocked<span className='text-red-600'>.</span></h2>
                        <p className='text-slate-800 lg:text-2xl text-xl font-body mt-8 lg:text-right'>Client come to us with ambitious top-line goals. <br />We help them realize their growth goals with cutting-edge know-<br />how and innovative ideas. </p>
                    </div>
                </div>
            </div>
            <div className="w-full h-16 text-dark text-white bg-blue-950 ">
                <div className="flex items-center overflow-hidden lg:px-24 lg:w-3/5 p-4">
                    <AiFillCaretRight className='text-red-500' /><p className='font-bold uppercase '>Featured Insights</p>
                </div>
            </div>
            <div className="w-full flex items-center justify-between">
                <div className='w-full h-auto lg:flex'>
                    {
                        data && data.map((dt, index) => (
                            <>
                                <div className="relative w-full lg:h-[400px] lg:max-h-[80vh] lg:overflow-hidden" key={index}>
                                    <div className='w-full relative lg:min-h-[35rem] min-h-[21rem]'>
                                        <Image
                                            src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : '/../../assets/images/featured1.jpg'}
                                            alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                            fill
                                            className='w-full h-auto brightness-50'
                                            objectFit="cover"
                                            sizes="100vw"
                                        />
                                        <div className="absolute lg:px-14 lg:w-full h-auto lg:grid items-end lg:py-10 p-4">
                                            <div className="w-4/5 text-white" >
                                                {
                                                    dt ? dt.categories.nodes.map((dt2, i2) => (
                                                        <>
                                                            <h2 className='text-xs font-title uppercase lg:mt-48 mt-40' key={i2}>{dt2.name ? dt2.name : ""}</h2>
                                                            <span className='mt-10 lg:text-xl text-[18px] sm:font-medium font-title text-slate-50 uppercase'>{dt ? dt.title : ""}</span>
                                                            <div>
                                                                <Link href={"/" + `${dt.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                                                            </div>
                                                        </>
                                                    ))
                                                        : ""
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* 
                                <div className="text-white lg:flex bg-no-repeat bg-cover w-full lg:h-2/3" style={{ backgroundImage: `url('${dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/assets/images/featured1.jpg"}')`, width: '100%', height: '400px' }} key={index}>
                                    <div className="lg:px-14 lg:w-full h-auto lg:grid items-end lg:py-10 p-4">
                                        <div className="w-full">
                                            {
                                                dt ? dt.categories.nodes.map((dt2, i2) => (
                                                    <>
                                                        <h2 className='text-xs font-title uppercase' key={i2}>{dt2.name ? dt2.name : ""}</h2>
                                                        <span className='mt-10 text-2xl mb-4 sm:font-medium font-title text-slate-50 uppercase'>{dt ? dt.title : ""}</span>
                                                        <div>
                                                            <Link href={"/" + `${dt.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                                                        </div>
                                                    </>
                                                ))
                                                    : ""
                                            }


                                        </div>
                                    </div>
                                </div> */}
                            </>
                        ))
                    }
                    {
                        dataip && dataip.map((dtip, index) => (
                            <>
                                <div className="relative w-full lg:h-[400px] lg:max-h-[80vh] lg:overflow-hidden" key={index}>
                                    <div className='w-full relative lg:min-h-[35rem] min-h-[21rem]'>
                                        <Image
                                            src={dtip.featuredImage !== null ? dtip.featuredImage.node.sourceUrl : '/../../assets/images/featured1.jpg'}
                                            alt={dtip.featuredImage !== null ? dtip.featuredImage.node.altText : "Picture of the author"}
                                            fill
                                            className='w-full h-auto brightness-50'
                                            objectFit="cover"
                                            sizes="100vw"
                                        />
                                        <div className="absolute lg:px-14 lg:w-full h-auto lg:grid items-end lg:py-10 p-4">
                                            <div className="w-4/5 text-white" >
                                                {
                                                    dtip ? dtip.categories.nodes.map((dtip2, i2) => (
                                                        <>
                                                            <h2 className='text-xs font-title uppercase lg:mt-48 mt-36' key={i2}>{dtip2.name ? dtip2.name : ""}</h2>
                                                            <span className='mt-10 lg:text-xl text-[18px] sm:font-medium font-title text-slate-50 uppercase'>{dtip ? dtip.title : ""}</span>
                                                            <div>
                                                                <Link href={"/" + `${dtip.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                                                            </div>
                                                        </>
                                                    ))
                                                        : ""
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </>
                        ))
                    }
                    {
                        datains && datains.map((dtins, index) => (
                            <>
                                <div className="relative w-full lg:h-[400px] lg:max-h-[80vh] lg:overflow-hidden" key={index}>
                                    <div className='w-full relative lg:min-h-[35rem] min-h-[21rem]'>
                                        <Image
                                            src={dtins.featuredImage !== null ? dtins.featuredImage.node.sourceUrl : '/../../assets/images/featured1.jpg'}
                                            alt={dtins.featuredImage !== null ? dtins.featuredImage.node.altText : "Picture of the author"}
                                            fill
                                            className='w-full h-auto brightness-50'
                                            objectFit="cover"
                                            sizes="100vw"
                                        />
                                        <div className="absolute lg:px-14 lg:w-full h-auto lg:grid items-end lg:py-10 p-4">
                                            <div className="w-4/5 text-white" >
                                                {
                                                    dtins ? dtins.categories.nodes.map((dtip2, i2) => (
                                                        <>
                                                            <h2 className='text-xs font-title uppercase lg:mt-48 mt-36' key={i2}>{dtip2.name ? dtip2.name : ""}</h2>
                                                            <span className='mt-10 lg:text-xl text-[18px] sm:font-medium font-title text-slate-50 uppercase'>{dtins ? dtins.title : ""}</span>
                                                            <div>
                                                                <Link href={"/" + `${dtins.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                                                            </div>
                                                        </>
                                                    ))
                                                        : ""
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                {/* <div className="text-white lg:flex bg-no-repeat bg-cover w-full lg:h-2/3" style={{ backgroundImage: `url('${dtins.featuredImage !== null ? dtins.featuredImage.node.sourceUrl : "/assets/images/featured1.jpg"}')`, width: '100%', height: '400px' }} key={index}>
                                    <div className="lg:px-14 lg:w-full h-auto lg:grid items-end lg:py-10 p-4">
                                        <div className="w-full">
                                            {
                                                dtins ? dtins.categories.nodes.map((dtins2, iip2) => (
                                                    <>
                                                        <h2 className='text-xs font-title uppercase' key={iip2}>{dtins2.name ? dtins2.name : ""}</h2>
                                                        <span className='mt-10 text-2xl mb-4 sm:font-medium font-title text-slate-50 uppercase'>{dtins ? dtins.title : ""}</span>
                                                        <div>
                                                            <Link href={"/" + `${dtins.slug}`} className='buttonsm bg-red-500 text-white text-sm rounded mt-4'> Read Now</Link>
                                                        </div>
                                                    </>
                                                ))
                                                    : ""
                                            }


                                        </div>
                                    </div>
                                </div> */}
                            </>
                        ))
                    }
                </div>
            </div>
        </section>
    )
}
