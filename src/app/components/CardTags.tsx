'use client'

import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import Image from 'next/image'

interface MenuProps {
    items: Data
    keyword: number[]
    slug: string
}

interface Data {
    nodes: postItem[]
    pageInfo: {
        endCursor: String
        hasNextPage: Boolean
        hasPreviousPage: Boolean
    }
}

interface postItem {
    title: String
    slug: String
    seo: {
        metaDesc: String
    }
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    categories: {
        nodes: categori[]
    }
}

interface categori {
    parent: {
        node: {
            name: string
        }
    }
}

const CardTags: React.FC<MenuProps> = ({ items: initialItems, keyword, slug }) => {
    const [items, setItems] = useState(initialItems)
    const [isLoading, setIsLoading] = useState(false)
    const getSearch = async (params: number[], after: String) => {
        try {
            setIsLoading(true)
            const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
                cache: 'no-store',
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    query: `
                    query getPostByCategoryId($first: Int!, $after: String) {
                        posts(where: {tagIn: [${params}], status: PUBLISH, orderby: {field: DATE, order: DESC}},first: $first, after: $after) {
                          nodes {
                            title
                            slug
                            seo {
                              metaDesc
                            }
                            featuredImage {
                                node {
                                    altText
                                    title
                                    sourceUrl
                                }
                            }
                            categories {
                                nodes {
                                    parent {
                                        node {
                                            name
                                        }
                                    }
                                }
                            }
                          }
                          pageInfo {
                            endCursor
                            hasNextPage
                            startCursor
                          }
                        }
                    }`,
                    variables: {
                        first: 3,
                        after: after
                    }
                })
            })
            if (!resp.ok) {
                throw new Error('Failed to fetch data')
            }
            const { data } = await resp.json()
            const newData = {
                ...data.posts,
                nodes: [...items.nodes, ...data.posts.nodes]
            }
            setItems(newData)
        } catch (error) {
            console.error('Error fetching data')
        } finally {
            setIsLoading(false);
        }
    }

    const handleLoadMore = (e: React.FormEvent) => {
        e.preventDefault()
        getSearch(keyword, items.pageInfo.endCursor)
    }
    return (
        <>
            <div className="h-auto lg:w-full grid lg:grid-cols-3 grid-cols-1 lg:px-0 lg:mt-4 mt-8 bg-white gap-x-8 gap-y-8">
                {items ? items.nodes.map((dt, i) => (
                    dt.slug !== slug
                        ?
                        <>
                            <div className="w-full h-auto justify-between lg:flex lg:mt-4" key={i}>
                                <Link href={`/${dt.slug}`} scroll={true} className="relative lg:flex lg:flex-col mt-6 text-gray-700 bg-white  bg-clip-border w-96" >
                                    <div className="w-full h-auto" >
                                        <Image
                                            src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/featured3.jpg"}
                                            alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                            width={200}
                                            height={112.5}
                                            quality={90}
                                            className='w-full h-[220px]'
                                            sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                        />
                                    </div>
                                    <div className="w-full h-auto gap-4">
                                        <div className="w-full h-auto ">
                                            <div className="lg:text-md text-[14px] text-slate-800 mt-4 font-title uppercase">{dt.categories !== null ? dt.categories.nodes.map((c, i) => (<><span key={i}>{c.parent.node.name}</span></>)) : ""} </div>
                                            <div className="lg:text-xl text-lg hover:underline font-titlea text-[#1B1072] uppercase">{dt ? dt.title : ""}</div>
                                            <h2 className='text-base font-body text-slate-800 lg:mt-4 mt-2'>{dt ? dt.seo.metaDesc.slice(0, 150) + " ..." : ""}</h2>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </>
                        : ""

                ))
                    : ""
                }
            </div>
            <div className='py-12'>
                {
                    items.pageInfo.hasNextPage && (
                        isLoading ?
                        <div className='container'>
                            <div className="circle"></div>
                        </div>                        
                        : <>
                        <div className='bg-white'>
                            <form method="post" className='grid p-4 justify-items-center' onSubmit={handleLoadMore}>
                                <div>
                                    <button type="submit" disabled={isLoading} className='bg-[#1B1072] text-white p-4 lg:text-lg text-sm uppercase font-title hover:bg-blue-900 hover:scale-100 duration-100' >
                                        {isLoading ? 'Loading...' : 'Find More'}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </>
                    )
                }
            </div>
            
        </>
    )
}

export default CardTags