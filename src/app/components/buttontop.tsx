'use client'
import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import { FaArrowDown, FaArrowUp  } from 'react-icons/fa';

const ScrollButton = () => {
  const [showScroll, setShowScroll] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 300) {
        setShowScroll(true);
      } else {
        setShowScroll(false);
      }
    };

    window.addEventListener('scroll', handleScroll);

    // Cleanup
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };
  const scrollToBottom = () => {
    window.scrollTo({ top: window.scrollY + window.innerHeight, behavior: 'smooth' });
  };

  return (
    <div className='animate-bounce text-6xl z-50' style={{ display: showScroll ? 'flex' : 'block' }}>
      <button >
        {showScroll
          ? 
          // <Image
          //   src={'/../../assets/images/arrow/Up Arrow Black.png'}
          //   alt={"Picture of the author"}
          //   width={50}
          //   height={50}
          //   quality={100}
          //   className='w-full lg:h-auto h-10 font-semibold'
          //   onClick={scrollToTop}
          // />
          <FaArrowUp  onClick={scrollToTop} className='text-slate-700 text-4xl z-0'/> 
          : 
          // <Image
          //   src={'/../../assets/images/arrow/Down Arrow Black.png'}
          //   alt={"Picture of the author"}
          //   width={50}
          //   height={50}
          //   quality={100}
          //   className='w-full lg:h-auto h-10 font-semibold'
          //   onClick={scrollToBottom}
          // />
          
          <FaArrowDown onClick={scrollToBottom} className='text-4xl text-slate-700 z-0' />
          }
      </button>
    </div>
  );
};

export default ScrollButton;