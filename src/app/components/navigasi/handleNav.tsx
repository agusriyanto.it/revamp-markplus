"use client"

import Link from "next/link"
import { useState } from "react"
import { AiOutlineArrowLeft, AiOutlineClose, AiOutlineMenu, AiOutlineSearch } from "react-icons/ai"
import { useRouter } from "next/navigation";

interface Caty {
    children: {
        nodes: childrens[];
    };
    name: string,
    slug: string,
    categoryId: number,
    link: string,
}

interface childrens {
    id: number,
    name: string,
    slug: string,
    children: {
        nodes: childs[]
    }
}

interface childs {
    name: string
    slug: string
    children: {
        nodes: childs2[]
    }
}

interface childs2 {
    name: string
    slug: string
}

interface MenuProps {
    items: Caty[]
    ind: Caty[]
    feat: Caty[]
    about: Caty[]
}

interface Category {
    name: string
    categoryId: number
    children: {
        nodes: childs[]
    }
}


const MenuNav: React.FC<MenuProps> = ({ items, ind, feat, about }) => {
    const [menuOpen, setMenuOpen] = useState(false)
    const [itemOpen, setItemOpen] = useState(false)
    const [formOpen, setFormOpen] = useState(false)
    const [keyword, setKeyword] = useState('')
    const [key, setKey] = useState<Category[]>([])
    const router = useRouter()

    const handleNav = () => {
        setMenuOpen(!menuOpen)
    }
    const handleShow = () => {
        setFormOpen(!formOpen)
    }
    const onSubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        router.push(`/search/${keyword}`)
        handleShow()
    }
    const handleDropdown = async (slug: string) => {
        try {
            const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
                cache: 'no-store',
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    query: `
                    query getTagsBySlug {
                        categories(where: {slug: "${slug}"}) {
                            nodes {
                                categoryId
                                name
                                children(first: 50) {
                                    nodes {
                                        name
                                        slug
                                        children {
                                            nodes {
                                                name
                                                slug
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      }`
                })
            })
            if (!resp.ok) {
                throw new Error('Failed to fetch data')
            }
            const { data } = await resp.json()
            const categories: Category[] = data.categories.nodes
            setKey(categories)
            setItemOpen(!itemOpen)
        } catch (error) {
            console.log("error fetching data : " + error)
        }

    }

    return (
        <>
            <div onClick={handleNav} className="sm:hidden cursor-pointer">
                <AiOutlineMenu className="text-black" />
            </div>
            <div className={
                menuOpen
                    ? "fixed left-0 top-0 w-3/4 sm:hidden h-screen overflow-y-auto bg-white p-10 ease-in duration-500"
                    : "fixed left-[-100%] top-0 p-10  h-screen ease-in duration-500"
            }>
                <div className="flex w-full items-center justify-end">
                    <div onClick={handleNav} className="cursor-pointer">
                        <AiOutlineClose className="text-black" />
                    </div>
                </div>
                <div className="grid gap-4">
                    <ul>
                        {
                            ind && ind.map((caty, index) => (
                                <>

                                    {
                                        caty.name !== 'Careers'
                                            ?
                                            <>
                                                <li className={"hoverable p-5 font-title"} key={index}>
                                                    <Link href='#' onClick={() => handleDropdown(caty.slug)} className="text-slate-800 uppercase hover:text-blue-800 mr-4">{caty.name ? caty.name : ""}</Link>
                                                </li>
                                            </>
                                            : ""
                                    }
                                </>
                            ))
                        }
                        {
                            items && items.map((caty, index) => (
                                <>

                                    {
                                        caty.name !== 'Careers'
                                            ?
                                            <>
                                                <li className={"hoverable p-5 font-title"} key={index}>
                                                    <Link href='#' onClick={() => handleDropdown(caty.slug)} className=" text-slate-800 uppercase hover:text-blue-800 mr-4">{caty.name ? caty.name : ""}</Link>
                                                </li>
                                            </>
                                            : ""
                                    }
                                </>
                            ))
                        }
                        {
                            feat && feat.map((caty, index) => (
                                <>
                                    {
                                        caty.name !== 'Careers'
                                            ?
                                            <>
                                                <li className={"hoverable p-5 font-title"} key={index}>
                                                    <Link href={caty.name !== 'Featured Insights' ? "#" : "/featuredinsights"} className=" text-slate-800 uppercase hover:text-blue-800 mr-4" onClick={() => handleDropdown(caty.slug)}>{caty.name ? caty.name : ""}</Link>
                                                </li>
                                            </>
                                            : ""
                                    }
                                </>
                            ))
                        }
                        {
                            about && about.map((caty, index) => (
                                <>

                                    {
                                        caty.name !== 'Careers'
                                            ?
                                            <>
                                                <li className={"hoverable p-5 font-title"} key={index}>
                                                    <Link href={caty.name !== 'Featured Insights' ? "#" : "/featuredinsights"} className=" text-slate-800 uppercase hover:text-blue-800 mr-4" onClick={() => handleDropdown(caty.slug)}>{caty.name ? caty.name : ""}</Link>
                                                </li>
                                            </>
                                            : ""
                                    }
                                </>
                            ))
                        }
                        <div onClick={() => { handleNav(), handleShow() }} className="cursor-pointer items-center">
                            <li className="p-4 mt-10 inline-flex text-md uppercase font-title text-black" >Search <AiOutlineSearch className="text-xl text-black ml-2 " /></li>
                        </div>
                        <div className={
                            formOpen
                                ? "fixed left-0 top-0 w-full z-10 bg-slate-800 bg-opacity-40 h-screen ease-in duration-500"
                                : "fixed left-[-100%] top-0 h-28 ease-in duration-500"
                        }>
                            <div className="flex w-full h-28 lg:px-24 px-2 bg-white items-center justify-between">
                                <form className="mt-2 w-full flex gap-4 z-0">
                                    <div className="w-full flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600">
                                        <input
                                            type="text"
                                            name="search"
                                            id="first-name"
                                            value={keyword}
                                            onChange={(e) => setKeyword(e.target.value)}
                                            placeholder="Type to search..."
                                            className="flex-1 lg:text-4xl text-xl font-body py-2 border-b-2 border-gray-400 focus:border-[#1B1072] 
                                text-gray-600 placeholder-gray-400
                                outline-none"
                                        />
                                    </div>
                                    <button type="submit" onClick={onSubmit}><AiOutlineSearch className="text-2xl text-black" /></button>
                                </form>
                                <div onClick={handleShow} className="flex cursor-pointer items-center p-4">
                                    <AiOutlineClose className="text-2xl text-black" />
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>

                {/* Menu Item */}
                <div className={
                    itemOpen
                        ? "fixed left-0 top-0 w-full sm:hidden h-screen overflow-y-auto bg-white p-10 ease-in duration-300"
                        : "fixed left-[-100%] top-0 p-10  h-screen ease-in duration-300"
                }>
                    <div className="flex w-full items-center justify-end">
                        <div onClick={() => handleDropdown("false")} className="cursor-pointer">
                            <AiOutlineArrowLeft className="text-black text-xl" />
                        </div>
                    </div>
                    <div className="grid gap-4 mt-4">
                        {key.map((category, i) => (
                            category.children.nodes.map((child, j) => (
                                <>
                                    <ul >
                                        <li className="p-3 font-title uppercase" key={i}>
                                            {
                                                category.categoryId !== 9
                                                    ? child.name ?
                                                        <>
                                                            <Link href={category.name !== "Featured Insights"
                                                                ? category.name === "About Us"
                                                                    ? '/about/' + child.slug
                                                                    : '../' + child.slug
                                                                : '/featured-insight/' + child.slug} onClick={() => { handleNav(), handleDropdown("false") }} className="text-dark text-slate-800 uppercase hover:text-blue-900 mr-4" key={j}>{child.name}
                                                            </Link>
                                                        </> : ""
                                                    :
                                                    <>
                                                        <div className="items-center">
                                                            <span className='text-dark text-xl font-bold text-slate-800 uppercase'>{child.name ? child.name : ""}</span>
                                                            <div className="border mt-2 border-solid border-slate-400 opacity-50"></div>
                                                            {child.children.nodes.map((dt3, i3) => (
                                                                <>
                                                                    <ul className="grid lg:mt-0 gap-4 w-full border-gray-600 pb-3 pt-3 lg:pt-3" ></ul>
                                                                    <Link onClick={() => { handleNav(), handleDropdown("false") }} href={'/' + dt3.slug} className="text-dark text-slate-800 uppercase hover:text-blue-900 mr-4" key={i3}>{dt3.name}</Link>
                                                                </>
                                                            ))
                                                            }
                                                        </div>
                                                    </>

                                            }

                                        </li>
                                    </ul>
                                </>
                            ))
                        ))}
                    </div>
                </div>
            </div>
        </>
    )
}

export default MenuNav