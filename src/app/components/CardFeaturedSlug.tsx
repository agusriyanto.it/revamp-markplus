'use client'

import Link from 'next/link'
import React, { useState } from 'react'
import Image from 'next/image'

interface MenuProps {
    items: Data
    keyword: string
}

interface Data {
    parent: {
        node: {
            name: string
        };
    }
    description: string
    slug: string
    name: string
    title: string
    posts: {
        nodes: post[]
        pageInfo: {
            endCursor: String
            hasNextPage: Boolean
            hasPreviousPage: Boolean
        }

    }
    children: {
        nodes: childrens[];
    }
}

interface post {
    title: string
    slug: string
    name: string
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    seo: {
        metaDesc: string
    }
}

interface childrens {
    id: number
    name: string
    title: string
    posts: {
        nodes: post[]
    }
}

const CardFeaturedSlug: React.FC<MenuProps> = ({ items: initialItems, keyword }) => {
    const [items, setItems] = useState(initialItems)
    const [isLoading, setIsLoading] = useState(false)
    const getSearch = async (params: String, after: String) => {
        try {
            setIsLoading(true)
            const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
                cache: 'no-store',
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    query: `
                    query getSlug($first: Int!, $after: String){
                        category(id: "${params}", idType: SLUG) {
                            name
                            slug
                            parent {
                                node {
                                    name
                                }
                            }
                            posts(where: {status: PUBLISH},first: $first, after: $after) {
                                nodes {
                                    title
                                    featuredImage {
                                        node {
                                        altText
                                        title
                                        sourceUrl
                                        }
                                    }
                                    seo {
                                        metaDesc
                                    }
                                    slug
                                }
                                pageInfo {
                                    endCursor
                                    hasNextPage
                                    hasPreviousPage
                                    startCursor
                                }
                            }
                        }
                      }`,
                    variables: {
                        first: 6,
                        after: after
                    }
                })
            })
            if (!resp.ok) {
                throw new Error('Failed to fetch data')
            }
            const { data } = await resp.json()
            const newData = {
                ...items,
                posts: {
                    ...items.posts,
                    nodes: [...items.posts.nodes, ...data.category.posts.nodes],
                    pageInfo: data.category.posts.pageInfo
                }
            }
            setItems(newData)
        } catch (error) {
            console.error('Error fetching data')
        } finally {
            setIsLoading(false);
        }
    }

    const handleLoadMore = (e: React.FormEvent) => {
        e.preventDefault()
        getSearch(keyword, items.posts.pageInfo.endCursor)
    }
    return (
        <>
            <div className="h-auto lg:w-2/3 lg:grid lg:grid-cols-3 bg-white p-4 gap-4 lg:mb-24 mb-2">
                {items ? items.posts.nodes.map((dt, i) => (
                    dt.seo.metaDesc
                        ?
                        <>
                            <div className="w-full h-auto justify-between lg:flex mt-5" key={i}>
                                <Link href={'/' + dt.slug} className="relative lg:flex lg:flex-col mt-6 text-gray-700 bg-white bg-clip-border w-96" >
                                    <div className="w-full h-auto" >
                                        <Image
                                            src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                            alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                            width={200}
                                            height={112.5}
                                            quality={90}
                                            className='w-full h-[220px]'
                                            sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                        />
                                    </div>
                                    <div className="w-full h-auto gap-4">
                                        <div className="w-full h-auto ">
                                            <div className="text-md text-slate-800  mt-4 font-title uppercase">{items ? items.name : ""} </div>
                                            <div className="text-2xl font-titlea hover:underline text-[#1B1072] uppercase">{dt ? dt.title : ""}</div>
                                            <h2 className='text-xl font-body text-slate-800 mt-4'>{dt ? dt.seo.metaDesc.slice(0, 150) + " ..." : ""}</h2>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </>
                        : ""

                )) : ""}
            </div>
            {
                items.posts.pageInfo.hasNextPage && (
                    isLoading ? 
                    <div className='grid justify-items-center mb-20'>
                        <div className="circle"></div>
                    </div>
                    :
                    <form method="post" className='grid p-4 mb-10 justify-items-center' onSubmit={handleLoadMore}>
                        <div>
                            <button type="submit" disabled={isLoading} className='bg-[#1B1072] text-white p-4 text-md uppercase font-title hover:bg-blue-900 hover:scale-100 duration-100' >
                                {isLoading ? 'Loading...' : 'Find More'}
                            </button>
                        </div>
                    </form>
                )
            }
        </>
    )
}

export default CardFeaturedSlug