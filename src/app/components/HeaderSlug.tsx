'use client'

import React from 'react'
import Image from 'next/image'

interface MenuProps {
    items: {
        categories: {
            nodes: {
                categori: categori[]
            }
        }
        featuredImage: {
            node: {
                altText: string
                sourceUrl: string
                caption: string
            };
        }
        seo: {
            metaDesc: string
        }
        content: string
        slug: string
        title: string
        date: Date
    }
    data: datas[]
}

interface datas {
    parent: {
        node: {
            name: string
        };
    };
    description: string
    slug: string
    name: string
}

interface categori {
    name: string
    slug: string
    description: string
    parent: {
        node: {
            name: string
        }
    }
}


const HeaderSlug: React.FC<MenuProps> = ({ items, data }) => {

    return (
        <>
            <div className="text-white relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh' }}>
                <div className='w-full lg:min-h-[35rem] min-h-[21rem] parallax-img img'>
                    <Image
                        src={items ? (items.featuredImage !== null ? items.featuredImage.node.sourceUrl : '/../../assets/images/article3.jpg') : '/../../assets/images/article3.jpg'}
                        alt={items ? (items.featuredImage !== null ? items.featuredImage.node.altText : 'MarkPlus.Inc') : 'MarkPlus.Inc'}
                        fill
                        priority
                        placeholder="blur"
                        blurDataURL={`/../../assets/images/article3.jpg`}
                        className='w-full h-auto absolute top-0 left-0 brightness-75'
                        objectFit="cover"
                        style={{
                            objectFit: 'cover',
                            zIndex: '-1',
                        }}
                    />
                    <div className="absolute lg:top-40 top-32 lg:w-2/4 lg:left-24 left-4 ">
                        <div className=' grid items-center' >
                            <div className="items-center lg:w-3/4">
                                <h2 className='lg:text-5xl font-bold font-bannersm uppercase text-white'>{data ? data.map((dt, i) => (<>{dt.parent.node.name}<span className='text-rose-700' key={i}>.</span></>)) : ""}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default HeaderSlug