import { getPostsBySlug } from '@/app/libs/api'
import Accordion from '../Accordion'
import Image from 'next/image'
import TopPage from '../TopPage'

interface Caty {
    content: string
    slug: string
    title: string
}

interface MenuProps {
    slug: string
}

const BePartOfUs = async () => {
    const data: Caty[] = await getPostsBySlug()
    return (
        <>
            <TopPage />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
            <div className="text-white relative w-full h-auto">
                <div className='w-full relative lg:min-h-[46rem] min-h-[20rem]'>
                    <Image
                        src={'/../../assets/images/bpou.jpg'}
                        alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        quality={100}
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                            objectFit: 'cover',
                        }}
                    />
                    <div className="absolute lg:top-40 top-28 lg:w-2/4 lg:left-24 left-4">
                        <div className=' grid items-center' >
                            <div className="items-center lg:w-3/4">
                                <h2 className='lg:text-5xl font-bold font-bannersm uppercase text-[#1B1072]'>BE PART OF US<span className="text-red-600">.</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className=" max-w-[1440px] w-full lg:left-24 left-6">
                <div className='w-full items-center lg:py-14 py-4 h-auto justify-between p-4 bg-white'>
                    {/* <div className='lg:px-20 w-full'>
                        <h2 className='text-6xl uppercase font-banner text-[#1B1072]'>BE PART OF US<span className="text-red-600">.</span></h2>
                    </div> */}
                    <div className='lg:px-20 w-full mb-6'>
                        <h2 className='lg:text-4xl text-xl font-title uppercase text-slate-700'>Find Your Opportunity</h2>
                        <h2 className='lg:text-2xl text-md font-body text-slate-700 lg:mt-4 mt-2'>Being a renowned firm with international networks, we have partnerships with some of the world’s leading figures in the marketing and business fields. Our clients from multinational companies and various governments further extend our far-reaching networks.overnments for years.</h2>
                        <h2 className='lg:text-2xl text-md font-body text-slate-700 mt-4'>As ASEAN’s leading marketing consulting provider, you will gain regional working exposure by collaborating on projects from regional clients! We are also constantly on the look out for associates who have the ability to work in a cross-culture environment for job rotations and leadership positions.</h2>
                    </div>
                    {
                        data && data.map((dt, index) => (
                            <div className='lg:px-20 w-full lg:flex items-center justify-between py-2 gap-4' key={index}>
                                <div className='py-2 bg-gray-200 rounded-lg w-full'>
                                    <Accordion title={dt.title} content={dt.content} />
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </main>
        </>
       
    )
}
export default BePartOfUs