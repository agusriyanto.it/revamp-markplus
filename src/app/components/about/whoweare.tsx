import Image from 'next/image'
import TopPage from '../TopPage'

export default async function WhoWeAre() {
    return (
        <>
            <TopPage />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
                <div className=" max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
                    <div className='w-full items-center lg:py-40 py-36 h-auto justify-between p-4 bg-white'>
                        <div className='lg:px-20 w-full'>
                            <h2 className='text-6xl uppercase font-banner text-[#1B1072]'>Who We Are<span className="text-red-600">.</span></h2>
                        </div>
                        <div className='lg:px-20 w-full lg:mt-20 mt-10'>
                            <h2 className='lg:text-2xl text-[18px] text-black font-body'>MarkPlus is Indonesia’s premiere and pioneering marketing consulting company, founded in 1990, 1st of May, by world marketing guru Hermawan Kartajaya. MarkPlus serves a large variety of corporate, state-owned, domestic and multi-national firms, uncovering strategic insights and tactical breakthroughs, to help them meet their most pertinent goals.</h2>
                        </div>
                        <div className='lg:px-20 w-full lg:flex items-center justify-between py-8 gap-4'>
                            <div
                                className="relative grid h-[40rem] w-full max-w-[28rem] flex-col items-center justify-between overflow-hidden rounded-xl bg-white bg-clip-border text-center text-gray-700 mt-4">
                                <div
                                    className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-[url('/../../assets/images/constant-Innovation.jpg')] bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
                                    <div className="absolute inset-0 w-full h-full to-bg-black-10 bg-gradient-to-t from-black/80 via-black/50"></div>
                                </div>
                                <div className="relative p-6 px-6 py-14 md:px-12">
                                    <h2 className="mb-6 block font-sans lg:text-4xl text-2xl font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                                        Constant Innovation in Marketing
                                    </h2>
                                    <h5 className="block mb-4 font-sans lg:text-xl text-[18px] antialiased leading-snug tracking-normal font-body text-white">
                                        Founded by Hermawan Kartajaya, a world-class marketing guru, MarkPlus is constantly inventing new ways to approach the ever-changing market.
                                    </h5>
                                </div>
                            </div>
                            <div
                                className="relative grid h-[40rem] w-full max-w-[28rem] flex-col items-center justify-between overflow-hidden rounded-xl bg-white bg-clip-border text-center text-gray-700 mt-4">
                                <div
                                    className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-[url('/../../assets/images/featured1.jpg')] bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
                                    <div className="absolute inset-0 w-full h-full to-bg-black-10 bg-gradient-to-t from-black/80 via-black/50"></div>
                                </div>
                                <div className="relative p-6 px-6 py-14 md:px-12">
                                    <h2 className="mb-6 block font-sans lg:text-4xl text-2xl font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                                        Unparalleled Understanding of the region
                                    </h2>
                                    <h5 className="block mb-4 font-sans lg:text-xl text-[18px] antialiased leading-snug tracking-normal font-body text-white">
                                        MarkPlus, with its truly nation-wide network of 13 cities, is the gateway into all regions of the growing Indonesian market
                                    </h5>
                                </div>
                            </div>
                            <div
                                className="relative grid h-[40rem] w-full max-w-[28rem] flex-col items-center justify-between overflow-hidden rounded-xl bg-white bg-clip-border text-center text-gray-700 mt-4">
                                <div
                                    className="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-[url('/../../assets/images/unlockgrow.jpg')] bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
                                    <div className="absolute inset-0 w-full h-full to-bg-black-10 bg-gradient-to-t from-black/80 via-black/50"></div>
                                </div>
                                <div className="relative p-6 px-6 py-14 md:px-12">
                                    <h2 className="mb-6 block font-sans lg:text-4xl text-2xl font-medium leading-[1.5] tracking-normal text-white font-title uppercase antialiased">
                                        Comperhensive Service across Functions
                                    </h2>
                                    <h5 className="block mb-4 font-sans lg:text-xl text-[18px] antialiased  leading-snug tracking-normal font-body text-white">
                                        MarkPlus comprises 4 integrated business units which able to provide a comprehensive service without the need to engage 3rd party vendors.
                                    </h5>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </main>
        </>
    )
}