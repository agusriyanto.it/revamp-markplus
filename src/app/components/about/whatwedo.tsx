import { getSlugCaty } from '@/app/libs/api'
import Link from 'next/link'
import Image from 'next/image'
import TopPage from '../TopPage'

interface Caty {
    name: string
    slug: string
    title: string
    description: string
    seo: {
        metaDesc: string
    }
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
}


interface MenuProps {
    slug: string
}

const WhatWeDo: React.FC<MenuProps> = async ({ slug }) => {
    const data: Caty[] = await getSlugCaty(slug)

    return (
        <>
        <TopPage />
        <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
            <div className=" max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
                <div className='w-full items-center lg:py-40 py-32 h-auto p-4 bg-white'>
                    <div className='lg:px-20 w-full'>
                        <h2 className='lg:text-6xl text-2xl uppercase font-banner text-[#1B1072]'>What We Do<span className="text-red-600">.</span></h2>
                    </div>
                    <div className='lg:px-20 w-full lg:mt-20 mt-2'>
                        <h2 className='lg:text-2xl text-black text-[18px] font-body'>To serves our clients in providing integrated solutions. MarkPlus is supported by 4 business in research, training, and consulting</h2>
                    </div>
                    <div className='lg:px-20 w-full gap-4 justify-between lg:py-8'>
                        {
                            data && data.map((dt, index) => (
                                <>
                                    <div className="w-full lg:max-w-full lg:flex mt-14">
                                        <div className="w-full h-auto" key={index}>
                                            <Link href={"/about/what-we-do/"+ dt.slug }><Image
                                                src={ dt ? dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : '/../../assets/images/featured3.jpg' : '/../../assets/images/featured3.jpg'}
                                                alt={"Picture of the author"}
                                                width={200}
                                                height={112.5}
                                                quality={100}
                                                className='w-full h-auto'
                                                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                            /></Link>
                                        </div>
                                        <div className="bg-white w-full rounded-b lg:rounded-b-none lg:rounded-r lg:ml-10 lg:p-4 flex flex-col justify-between leading-normal">
                                            <div className="lg:mb-8 mb-2 ">
                                                <div className="lg:text-4xl text-2xl font-titlea text-[#1B1072] uppercase mt-2 ">{dt ? dt.title : ""}.</div>
                                                <p className="text-black font-body lg:text-2xl text-[18px] mt-2">{dt ? dt.seo.metaDesc : ""}.</p>
                                            </div>
                                            <div className="flex items-center">
                                                <div className="text-sm">
                                                    <Link href={"/about/what-we-do/"+ dt.slug } className='buttonsm text-md'>Read More</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div className="relative flex flex-col border-2 bg-clip-border rounded-xl bg-[#ebebeb] hover:bg-white  text-dark shadow-gray-900/20 shadow-md w-full max-auto p-8 items-start justify-between mt-6" >
                                        <div
                                            className="relative lg:pb-8 m-0 lg:mb-4 mb-2 overflow-hidden text-start text-gray-700 bg-transparent rounded-none shadow-none bg-clip-border border-slate-500">
                                            <h1 className="flex justify-center gap-1 mt-2 font-sans antialiased font-normal tracking-normal text-black lg:text-3xl text-2xl">
                                                {dt ? dt.title : ""}
                                            </h1>
                                        </div>
                                        <div className="p-0">
                                            <h2 className='font-body text-black lg:text-xl text-md'>{dt ? dt.seo.metaDesc : ""}</h2>
                                        </div>
                                        <div className="p-0 lg:mt-6 mt-2">
                                            <Link href={"/about/what-we-do/" + dt.slug} className='buttonsm text-md'>Read More</Link>
                                        </div>
                                    </div> */}
                                </>
                            ))
                        }
                    </div>
                </div>
            </div>
        </main>
        </>
        
    )

}
export default WhatWeDo