'use client'
import Image from 'next/image'
import { AiFillCloseCircle } from 'react-icons/ai'
import Slider from 'react-slick'
import TopPage from '../TopPage';

export default function OurFacilities() {
    const settings = {
        dots: true,
        dotsClass: "slick-dots slick-thumb",
        infinite: true,
        speed: 200,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000
    };
    return (
        <>
            <TopPage />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
                <div className='max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6'>
                    <div className='w-full items-center py-40 h-auto justify-between p-4 bg-white'>
                        <div className='lg:px-20 w-full'>
                            <h2 className='text-6xl uppercase font-banner text-[#1B1072]'>OFFICES & FACILITIES<span className="text-red-600">.</span></h2>
                        </div>
                        <div className="lg:px-20 w-full lg:mt-20 mt-14">
                            <div className='w-full h-auto lg:flex'>
                                <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
                                    <div className='w-full'>
                                        <Image
                                            src={'/../../assets/images/o&s/office01.jpg'}
                                            alt={'office01'}
                                            width={200}
                                            height={112.5}
                                            className='w-full h-auto '
                                            objectFit="cover"
                                            priority
                                            sizes="(min-width: 808px) 50vw, 100vw"
                                            style={{
                                                objectFit: 'cover',
                                            }}

                                        />
                                    </div>
                                </div>
                                <div className="relative w-full h-auto lg:p-8">
                                    <div className='w-full h-auto items-center justify-between lg:p-4 mt-2'>
                                        <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] lg:mb-5 mb-2 font-titlea uppercase">Hermawan Kartajaya Town Hall</div>
                                        <div className="lg:text-xl text-md font-body text-slate-800">
                                            Hermawan Kertajaya Town Hall is a creative space with a modern and stylish ambiance. Within this space, you'll find Hermawan Kertajaya memorabilia, serving as an inspiration hub for generating new ideas from Gen Z, tailored to MarkPlus Inc.'s marketing concept. It's within this environment that the latest concept, CI-EL (Creativity - Innovation - Entrepreneurship - Leadership), was born.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="lg:px-20 w-full mt-20">
                            <div className='w-full h-auto lg:flex'>
                                <div className="relative w-full h-auto lg:p-8">
                                    <div className='w-full h-auto items-center justify-between lg:p-4 mb-2'>
                                        <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] lg:mb-5 mb-2 font-titlea uppercase">Philip Kotler Theatre Class</div>
                                        <div className="lg:text-xl text-md font-body text-slate-800">
                                            The Philip Kotler Theater Class, named in tribute to Prof. Philip Kotler, renowned as The Father of Modern Marketing and a distinguished Professor from the Kellogg School of Management, the world's top-ranked marketing institution. With a regular capacity of 70 attendees, this is adjusted to 30 during pandemics through the implementation of health protocols. This theater class serves as the campus for the collaborative master's program offered by SMB ITB & MarkPlus Institute: Strategic Marketing.
                                        </div>
                                    </div>
                                </div>
                                <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
                                    <div className='w-full'>
                                        <Image
                                            src={'/../../assets/images/o&s/office03.jpg'}
                                            alt={'office01'}
                                            width={200}
                                            height={112.5}
                                            className='w-full h-auto '
                                            objectFit="cover"
                                            priority
                                            sizes="(min-width: 808px) 50vw, 100vw"
                                            style={{
                                                objectFit: 'cover',
                                            }}

                                        />
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="lg:px-20 w-full mt-20">
                            <div className='w-full h-auto lg:flex'>
                                <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
                                    <div className='w-full'>
                                        <Image
                                            src={'/../../assets/images/o&s/office04.jpg'}
                                            alt={'office01'}
                                            width={200}
                                            height={112.5}
                                            className='w-full h-auto '
                                            objectFit="cover"
                                            priority
                                            sizes="(min-width: 808px) 50vw, 100vw"
                                            style={{
                                                objectFit: 'cover',
                                            }}

                                        />
                                    </div>
                                </div>
                                <div className="relative w-full h-auto lg:p-8">
                                    <div className='w-full h-auto items-center justify-between lg:p-4 mt-2'>
                                        <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] lg:mb-5 mb-2 font-titlea uppercase">Marketeers TV Studio</div>
                                        <div className="lg:text-xl text-md font-body text-slate-8000">
                                            The Marketeers TV Studio boasts fully-equipped audiovisual and lighting systems, along with a green screen and backdrop featuring contemporary aesthetics synonymous with Marketeers - Indonesia's premier marketing media known for its cool, inspiring, and progressive content.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="lg:px-20 w-full mt-20">
                            <div className='w-full h-auto lg:flex'>
                                <div className="relative w-full h-auto lg:p-8">
                                    <div className='w-full h-auto items-center justify-between lg:p-4 mb-2'>
                                        <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] lg:mb-5 mb-2 font-titlea uppercase">FGD room</div>
                                        <div className="lg:text-xl text-md font-body text-slate-8000">
                                            Our FGD room is meticulously designed to foster robust discussions and insightful observations. It combines a versatile discussion hall with customizable table settings and essential equipment like projectors and recording systems, creating an immersive collaborative experience. Additionally, the room includes a dedicated viewing area with a one-way mirror, enabling discreet client observation of the FGD process. This setup ensures confidentiality while offering stakeholders valuable firsthand insights.
                                        </div>
                                    </div>
                                </div>
                                <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
                                    <div className='w-full'>
                                        <Image
                                            src={'/../../assets/images/o&s/office02.jpg'}
                                            alt={'office01'}
                                            width={200}
                                            height={112.5}
                                            className='w-full h-auto '
                                            objectFit="cover"
                                            priority
                                            sizes="(min-width: 808px) 50vw, 100vw"
                                            style={{
                                                objectFit: 'cover',
                                            }}

                                        />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>

    )
}