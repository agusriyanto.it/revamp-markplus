"use client"

import { useState } from "react"
import Swal from "sweetalert2"

const HandleSub = () => {
    const handleSub = () => {
        Swal.fire({
            title: "Thanks ya",
            icon: "success"
        });
    }

    return (
        <>
            <div className="relative w-5/6" data-te-input-wrapper-init>
                <input type="email" name="name" id="name" placeholder="Email"
                    className="w-full border border-[#e0e0e0] bg-white py-3 px-6 text-base font-titles font-medium text-[#6B7280] outline-none focus:border-[#1B1072] focus:shadow-md"
                />
            </div>
            <div>
                <button onClick={handleSub} type="submit" data-te-ripple-init data-te-ripple-color="light"
                    className="w-auto h-12 px-6 text-indigo-100 transition-colors duration-150 bg-[#1B1072] font-title uppercase focus:shadow-outline hover:bg-blue-900">
                    Subscribe
                </button>
            </div>
        </>

    )
}

export default HandleSub