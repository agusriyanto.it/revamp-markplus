'use client'

import { useState } from "react";
import parseHtml from "../utils/parser";

interface AccordionProps {
    title: string;
    content: string;
}
const Accordion: React.FC<AccordionProps> = ({ title, content }) => {
    const [accordionOpen, setAccordionOpen] = useState(false)
    return (
        <>
            <div className="py-2 p-4">
                <button onClick={() => setAccordionOpen(!accordionOpen)} className="w-full justify-between flex lg:text-2xl font-bold text-md text-black">
                    <span>{title}</span>
                    {accordionOpen ? <span>-</span> : <span>+</span>}
                </button>
                <div className={`grid overflow-hidden transition-all duration-300 ease-in-out ${accordionOpen ? 'grid-rows-[1fr] opacity-100' : 'grid-rows-[0fr] opacity-0'}`}>
                    <div className='overflow-hidden'>
                        <div className='w-full h-auto p-4 lg:mt-6 mt-1 lg:px-12  text-black'>{parseHtml(content)}</div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Accordion