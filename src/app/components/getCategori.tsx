export async function getData() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
          query getCategory {
            categories(where: {parent: 0, order: ASC, orderby: TERM_ID}) {
              nodes {
                name
                slug
                children(where: {order: ASC, orderby: TERM_ID}, first: 100) {
                  nodes {
                    id
                    name
                    slug
                    children {
                      nodes {
                        name
                        slug
                      }
                    }
                  }
                }
              }
            }
          }`
      })
    })
    const { data } = await resp.json()
    return data.categories.nodes
  } catch (error) {
    console.error('Error fetching data:', error);
  }

}

export async function getCapabilites() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
              query getCategory {
                  categories(where: {orderby: TERM_ID, parent: 0, order: ASC, name: "capabilites"}) {
                    nodes {
                      name
                      slug
                      categoryId
                      link
                      children(where: {order: ASC, orderby: TERM_ID}) {
                        nodes {
                          id
                          name
                          slug
                        }
                      }
                    }
                  }
                }`
      })
    })
  
    const { data } = await resp.json()
    return data.categories.nodes
  } catch (error) {
    console.log(error)
  }  
}

export async function getCapabilities() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
              query getCategory {
                  categories(where: {orderby: TERM_ID, parent: 0, order: ASC, name: "capabilities"}) {
                    nodes {
                      name
                      slug
                      categoryId
                      children(where: {order: ASC, orderby: TERM_ID}) {
                        nodes {
                          id
                          name
                          slug
                          children {
                            nodes {
                              name
                              slug
                            }
                          }
                        }
                      }
                    }
                  }
                }`
      })
    })
  
    const { data } = await resp.json()
    return data.categories.nodes
  } catch (error) {
    console.log(error)
  }  
}

export async function getMenuCategory(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
              query getCategory {
                  categories(where: {orderby: TERM_ID, parent: 0, order: ASC, name: "${params}"}) {
                    nodes {
                      name
                      slug
                      categoryId
                      link
                      children(where: {order: ASC, orderby: TERM_ID}, first: 100) {
                        nodes {
                          id
                          name
                          slug
                        }
                      }
                    }
                  }
                }`
      })
    })
  
    const { data } = await resp.json()
    return data.categories.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getCapabilitiesHome(params:number) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getCapabilities {
          posts(
            where: {orderby: {field: DATE, order: DESC}, status: PUBLISH, categoryId: ${params}}
          ) {
            nodes {
              featuredImage {
                node {
                  altText
                  sourceUrl
                  caption
                }
              }
              seo {
                metaDesc
                title
              }
              slug
              status
              title
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
          }
        }`
      })
    })
  
    const { data } = await resp.json()
    return data.posts.nodes
  } catch (error) {
    console.log(error)
  }  
}