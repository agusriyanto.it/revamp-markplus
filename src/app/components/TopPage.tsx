'use client'

import React, { useEffect } from 'react'

const TopPage = () => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <div></div>
    )
}

export default TopPage
