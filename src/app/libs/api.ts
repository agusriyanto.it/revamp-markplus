import { after } from "node:test"

export async function getHeadlines() {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
            query getHeadlines {
              posts(where: {showFeatured: true, status: PUBLISH}) {
                  nodes {
                    featuredImage {
                      node {
                        altText
                        sourceUrl
                        caption
                      }
                    }
                    seo {
                      metaDesc
                      title
                    }
                    content
                    date
                    slug
                    status
                    title
                    categories {
                      nodes {
                        parent {
                          node {
                            name
                          }
                        }
                      }
                    }
                  }
                }
            }`
      })
    })
    if (!res.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await res.json()
    return data.posts.nodes
  } catch (error) {
    console.error('Error fetching data')
    return null
  }

}

export async function getSinglePg(params: string) {
  try {
    const caty = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-cache',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
            query getSinglePage {
                post(id: "${params}", idType: SLUG) {
                  content(format: RENDERED)
                  title
                  date
                  featuredImage {
                    node {
                      altText
                      sourceUrl
                      title
                    }
                  }
                  categories {
                    nodes {
                      name
                      slug
                      description
                      parent {
                        node {
                          name
                        }
                      }
                    }
                  }
                  tags {
                    nodes {
                      name
                      slug
                    }
                  }
                  seo {
                    metaDesc
                  }
                }
              }`
      })
    })
    const { data } = await caty.json()
    return data.post
  } catch (error) {
    console.log(error)
  }

}

export async function getPostslugs() {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
          query getPostSlugs{
            posts {
              nodes{
                slug
              }
            }
          }`
      })
    })

    const { data } = await res.json()
    return data.posts.nodes
  } catch (error) {
    console.log(error)
  }

}


export async function getIndustriesDesc() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getFeaturedInsights{
          posts(
            where: {categoryId: 3, orderby: {field: DATE, order: DESC}, status: PUBLISH}
          ) {
            nodes {
              featuredImage {
                node {
                  altText
                  sourceUrl
                  caption
                }
              }
              seo {
                metaDesc
                title
              }
              slug
              status
              title
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    return data.posts.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getIndustriesAsc() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getFeaturedInsights{
          posts(
            where: {categoryId: 3, orderby: {field: DATE, order: ASC}, status: PUBLISH}
          ) {
            nodes {
              featuredImage {
                node {
                  altText
                  sourceUrl
                  caption
                }
              }
              seo {
                metaDesc
                title
              }
              slug
              status
              title
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    return data.posts.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getSlugHome(params: string, after: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getSlug($first: Int!, $after: String){
          category(id: "${params}", idType: SLUG) {
            name
            slug
            parent {
              node {
                name
              }
            }
            posts(where: {status: PUBLISH},after: $after, first: $first) {
              nodes {
                title
                featuredImage {
                  node {
                    altText
                    title
                    sourceUrl
                  }
                }
                seo {
                  metaDesc
                }
                slug
              }
              pageInfo {
                endCursor
                hasNextPage
                hasPreviousPage
                startCursor
              }
            }
          }
        }`, variables: {
          first: 6,
          after: after
        }
      })
    })

    const { data } = await resp.json()
    return data.category
  } catch (error) {
    console.log(error)
  }

}


export async function getNameCategori(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getCategory {
          category(id: "${params}", idType: SLUG) {
            name
            description
            slug
            children {
              nodes {
                name
                description
                slug
              }
            }
          }
        }
        `
      })
    })

    const { data } = await resp.json()
    return data.category
  } catch (error) {
    console.log(error)
  }

}


// "Featured Insights"
export async function getMenubyName(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query menuFeaturedInsights { 
          categories(where: {name: "${params}"}) {
            nodes {
              children {
                nodes {
                  name
                  slug
                  description
                  posts {
                    nodes {
                      title
                      content
                      slug
                      featuredImage {
                        node {
                          altText
                          slug
                          sourceUrl
                        }
                      }
                      seo {
                        metaDesc
                        metaKeywords
                      }
                    }
                  }
                }
              }
            }
          }
        }}
        `
      })
    })

    const { data } = await resp.json()
    if (!data) {
      console.log("notfound")
    } else {
      return data.categories.nodes
    }
  } catch (error) {
    console.log(error)
  }
}

export async function getHomeFi(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
            query getFeaturedInsights {
                category(id: "${params}", idType: SLUG) {
                  children {
                    nodes {
                      name
                      slug
                      posts(first: 2, where: {status: PUBLISH, orderby: {field: DATE, order: DESC}}) {
                        edges {
                          node {
                            title
                            date
                            slug
                            featuredImage {
                              node {
                                altText
                                sourceUrl
                              }
                            }
                            seo {
                              metaDesc
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            `
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.category.children.nodes
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}

export async function getIndustries(params: number) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getFeaturedInsights {
          posts(where: {categoryId: ${params}, status: PUBLISH}, first: 40) {
            nodes {
              title
              slug
              seo {
                metaDesc
              }
              featuredImage {
                node {
                  altText
                  sourceUrl
                }
              }
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
          }
        }
            `
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.posts.nodes
  } catch (error) {
    console.error('Error fetching data')
    return null
  }

}

export async function getPostBySlugFi(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
              query getSinglePage {
                category(id: "${params}", idType: SLUG) {
                  name
                  posts(where: {status: PUBLISH, orderby: {field: DATE, order: DESC}}, first: 4) {
                    edges {
                      node {
                        title
                        slug
                        featuredImage {
                          node {
                            altText
                            sourceUrl
                          }
                        }
                        seo {
                          metaDesc
                        }
                      }
                    }
                  }
                }
              }
            `
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.category.posts.edges
  } catch (error) {
    console.error('Error fetching data')
    return null
  }

}

export async function getSlugCaty(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getFeaturedInsights {
          category(id: "${params}", idType: SLUG) {
            posts {
              nodes {
                slug
                title
                content
                seo {
                  metaDesc
                }
                featuredImage {
                  node {
                    altText
                    title
                    sourceUrl
                  }
                }
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    return data.category.posts.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getParentBySlug(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getSlug {
          post(id: "${params}", idType: SLUG) {
            categories {
              nodes {
                name
                parent {
                  node {
                    name
                  }
                }
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    if (data.post === null) {
      console.log("notfound")
    } else {
      return data.post.categories.nodes
    }
  } catch (error) {
    console.log(error)
  }
}

export async function getPostsBySlug() {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getPostBySlug {
          category(id: "be-part-of-us", idType: SLUG) {
            name
            posts {
              nodes {
                title
                content
                status
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    return data.category.posts.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getLatesPostByName(params: number) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query GetLatestPost {
          posts(
            where: {categoryId: ${params}, orderby: {field: DATE, order: DESC}, status: PUBLISH}
            first: 1
          ){
            nodes {
              title
              slug
              date
              categories {
                nodes {
                  name
                  slug
                }
              }
              featuredImage {
                node {
                  altText
                  title
                  sourceUrl
                }
              }
              seo {
                metaDesc
              }
            }
          }
        }`
      })
    })

    const { data } = await resp.json()
    return data.posts.nodes
  } catch (error) {
    console.log(error)
  }
}

export async function getSearch(params: string, after: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getSearchPosts($searchQuery: String!,$first: Int!, $after: String) {
          posts(
            where: {search: $searchQuery,status: PUBLISH, orderby: {field: DATE, order: DESC}, categoryNotIn: "62"}
            first: $first, after: $after
          ) {
            nodes {
              title
              slug
              date
              featuredImage {
                node {
                  altText
                  sourceUrl
                }
              }
              seo {
                metaDesc
              }
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
              hasPreviousPage
            }
          }
        }`,
        variables: {
          searchQuery: params,
          first: 6,
          after: after
        }
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.posts
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}

export async function getIdCategoryBySlug(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getIdCategoryBySlug {
          category(id: "${params}", idType: SLUG) {
            children {
              nodes {
                categoryId
              }
            }
          }
        }`
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.category.children.nodes
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}

export async function getPostByCategoryId(params: number[], after: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getPostByCategoryId($first: Int!, $after: String) {
          posts(where: {categoryIn: [${params}], status: PUBLISH, orderby: {field: DATE, order: DESC}}, first: $first, after: $after) {
            nodes {
              title
              slug
              seo {
                metaDesc
              }
              featuredImage {
                node {
                    altText
                    title
                    sourceUrl
                }
            }
              categories {
                nodes {
                  name
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
              startCursor
            }
          }
        }`, variables: {
          first: 6,
          after: after
        }
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.posts
  } catch (error) {
    console.error('Error fetching data' + error)
    return null
  }
}

export async function getTagsBySlug(params: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getTagsBySlug {
          post(id: "${params}", idType: SLUG) {
            tags {
              nodes {
                name
                databaseId
              }
            }
          }
        }`
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.post.tags.nodes
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}

export async function getPostsByIdTags(params: number[], after: string) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getPostsByIdTags($first: Int!, $after: String) {
          posts(where: {tagIn: [${params}], status: PUBLISH,orderby: {field: DATE, order: DESC}},first: $first, after: $after) {
            nodes {
              title
              slug
              seo {
                metaDesc
              }
              featuredImage {
                node {
                  altText
                  sourceUrl
                  title
                }
              }
              categories {
                nodes {
                  parent {
                    node {
                      name
                    }
                  }
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
              startCursor
            }
          }
        }`, variables: {
          first: 3,
          after: after
        }
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.posts
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}

export async function getPagebyId(params: number) {
  try {
    const resp = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      cache: 'no-store',
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
        query getPolicyPriv {
          pageBy(pageId: ${params}) {
            title
            content
            seo {
              metaDesc
            }
            featuredImage {
              node {
                altText
                sourceUrl
              }
            }
          }
        }`
      })
    })
    if (!resp.ok) {
      throw new Error('Failed to fetch data')
    }
    const { data } = await resp.json()
    return data.pageBy
  } catch (error) {
    console.error('Error fetching data')
    return null
  }
}