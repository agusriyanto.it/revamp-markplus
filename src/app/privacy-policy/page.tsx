import React from 'react'
import TopPage from '../components/TopPage'
import { getPagebyId } from '../libs/api'
import parseHtml from '../utils/parser'

interface Data {
    title : string
    content : string
    seo : {        
        metaDesc : string
    }
    featuredImage : {
        node : {
          altText : string
          sourceUrl : string
        }
      }
}
const page = async () => {
    const data:Data = await getPagebyId(3)
    return (    
    <>
        <link rel="canonical" href={`https://www.staging.markplusinc.com/privacy-policy/`} key="canonical" />
            <title>{data ? data.title + " | MarkPlus.Inc" : "MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta name="keywords" content={data ? data.title : ""} />
            <meta name="news_keywords" content={data ? data.title : ""} />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : '/../../assets/images/meta-img.jpg'} />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:title" content={data ? data.title : ""} />
            <meta property="og:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/privacy-policy/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <meta name="twitter:title" content={data ? data.title : "MarkPlus.Inc"} />
            <meta name="twitter:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta name="twitter:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : "/../../assets/images/meta-img.jpg"} />
            <meta name="content_tags" content={data ? data.title : "MarkPlus.Inc"} />
            <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{ __html: JSON.stringify(data ? data.title : "MarkPlus.Inc") }}
            />
            <TopPage />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
            <div className=" max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
                <div className='w-full items-center lg:py-40 py-20 h-auto justify-between p-4 bg-white'>
                    <div className='lg:px-20 w-full'>
                        <h2 className='text-6xl uppercase font-banner text-[#1B1072] mt-16'>Privacy Policy<span className="text-red-600">.</span></h2>
                    </div>
                    <div className='lg:px-20 w-full lg:mt-28 mt-8'>
                        <section>
                            <div className="items-center text-2xl font-body text-slate-800" >{data ? parseHtml(data.content) : ""}</div>
                        </section>
                    </div>
                </div>
            </div>
        </main>
        </>
  )
}

export default page