import { getSinglePg, getSlugHome } from '@/app/libs/api'
import parse from 'html-react-parser'
import { AiFillCloseCircle } from 'react-icons/ai'
import Image from 'next/image'
import parseHtml from '@/app/utils/parser'
import TopPage from '@/app/components/TopPage'

interface categori {
    name: string
    slug: string
    description : string
    parent : {
        node : {
            name : string 
        }
    }
}

interface Caty {
    categories: {
        nodes: {
            categori : categori[]
        }
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
            caption: string
        };
    };
    seo :{
        metaDesc : string
    }
    content: string
    slug: string
    title: string
}

interface Cy {
    parent: {
        node: {
            name: string
        };
    };
    description: string
    slug: string
    name: string
}

export default async function Post({ params }: { params: { slug: string, after:string } }) {
    const data: Caty = await getSinglePg(params.slug)
    const dt: Cy = await getSlugHome(params.slug, params.after)
    return (
        <>
            <link rel="canonical" href={`https://www.staging.markplusinc.com/${params.slug}/`} key="canonical" />
            <title>{data ? data.title + " | MarkPlus.Inc" : "MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta name="keywords" content={data ? data.title : ""} />
            <meta name="news_keywords" content={data ? data.title : ""} />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : "/../../assets/images/meta-img.jpg"} />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:title" content={data ? data.title : ""} />
            <meta property="og:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/${params.slug}/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <meta name="twitter:title" content={data ? data.title : "MarkPlus.Inc"} />
            <meta name="twitter:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta name="twitter:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : "/../../assets/images/meta-img.jpg"} />
            <meta name="content_tags" content={data ? data.title : "MarkPlus.Inc"} />
            <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{ __html: JSON.stringify(data ? data.title : "MarkPlus.Inc") }}
            />
            <main className="flex-col sm:w-full sm:h-full mx-auto" >
            <>
                <TopPage />
                {
                    data !== null
                        ? <div className="text-white relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
                        <div className='w-full relative lg:min-h-[35rem] min-h-[21rem]'>

                        <Image
                        src={data ? (data.featuredImage !== null ? data.featuredImage.node.sourceUrl : '/../../assets/images/article3.jpg') : '/../../assets/images/article3.jpg'}
                        alt={data ? (data.featuredImage !== null ? data.featuredImage.node.altText : 'MarkPlus.Inc') : 'MarkPlus.Inc'}
                        fill
                        priority
                        className='w-full h-auto absolute top-0 left-0'
                        objectFit="cover"
                        style={{
                            objectFit: 'cover',
                            zIndex: '-1',
                        }}
                    />
                                <div className="absolute lg:top-40 top-36 lg:w-2/4 lg:left-24 left-4">
                                    <div className=' grid items-center' >
                                        <div className="items-center lg:w-3/4">
                                            <h2 className='font-body text-md uppercase text-slate-800'>About Us</h2>
                                            <h2 className='lg:text-5xl font-bold font-bannersm uppercase text-[#1B1072]'>What We Do</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : <div className="bg-white lg:flex bg-no-repeat bg-cover w-full lg:h-screen h-72 justify-center">
                            <div className='lg:py-28 ' >
                                <div className="overflow-hidden lg:px-24 p-6 items-center justify-between">
                                    <div className="items-center">
                                        <span className='font-title flex gap-4 text-4xl font-bold'><AiFillCloseCircle className='text-red-600' /> No Data Found</span>
                                        {/* <p className='mt-4 mb-4 sm:font-medium text-slate-50 text-sm'>{data.featuredImage.node.altText}</p>
                                 <h2 className='mb-20 text-6xl font-bold font-banner uppercase'>{data.featuredImage.node.caption}</h2> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                }

            </>
            <div className="w-full flex flex-col justify-center items-center lg:px-20 bg-white">
                <div className="max-w-4xl mx-auto lg:my-24 lg:mb-18 p-4">
                    <span className='font-body text-md uppercase'>{data ? dt ? dt.parent.node.name : "" : ""}</span>
                    <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] mb-10 font-titlea lg:w-2/4 uppercase">{data ? data.title : ""} </div>
                    <section>
                        <div className="items-center lg:text-xl text-[18px] font-body text-slate-800" >{data ? parseHtml(data.content) : ""}</div>
                    </section>
                </div>
            </div>
        </main>
        </>
        
    )
}