import BePartOfUs from '@/app/components/about/bepartofus'
import OurFacilities from '@/app/components/about/ourfacilities'
import WhatWeDo from '@/app/components/about/whatwedo'
import WhoWeAre from '@/app/components/about/whoweare'
import { getSinglePg, getSlugHome } from '@/app/libs/api'

interface categori {
  name: string
  slug: string
  description: string
  parent: {
      node: {
          name: string
      }
  }
}

interface Data {
  categories: {
      nodes: {
          categori: categori[]
      }
  }
  featuredImage: {
      node: {
          altText: string
          sourceUrl: string
          caption: string
      };
  }
  seo: {
      metaDesc: string
  }
  content: string
  slug: string
  title: string
  date: Date
}

interface post {
  title: string
  slug: string
  featuredImage: {
      node: {
          altText: string
          title: string
          sourceUrl: string
      }
  }
  seo: {
      metaDesc: string
  }
  categories: {
      nodes: categori[]
  }
}

export default async function Post({ params }: { params: { slug: string, after:string } }) {
    const data: Data = await getSlugHome(params.slug,params.after)
    return (
        <>
          <link rel="canonical" href={`https://www.staging.markplusinc.com/${params.slug}/`} key="canonical" />
          <title>{"About Us | MarkPlus.Inc"}</title>
          <meta name="googlebot-news" content="index,follow" />
          <meta name="googlebot" content="index,follow" />
          <meta name="robots" content="index, follow" />
          <meta name="keywords" content={data ? data.title : ""} />
          <meta name="news_keywords" content={data ? data.title : ""} />
          <meta property="og:type" content="article" />
          <meta property="og:site_name" content="www.markplusinc.com" />
          <meta property="og:creator" content="markplusinc" />
          <meta property="og:locale" content="id_ID" />
          <meta property="og:title" content={data ? data.title : ""} />
          <meta property="og:url" content={`https://www.staging.markplusinc.com/${params.slug}/`} />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@the_marketeers" />
          <meta name="twitter:creator" content="@the_marketeers" />
          <meta name="twitter:title" content={data ? data.title : "MarkPlus.Inc"} />
          <meta name="content_tags" content={data ? data.title : "MarkPlus.Inc"} />
          <main className="flex-col sm:w-full sm:h-full mx-auto" >
            <>
           
            { data ? 
            (() => {
            switch (data.slug) {
              case 'be-part-of-us':
                return (
                  <>
                    <BePartOfUs />
                  </>
                );
              case 'who-we-are':
                return (
                  <>
                    <WhoWeAre />
                  </>
                );
              case 'what-we-do':
                return (
                  <>
                    <WhatWeDo slug={data.slug}/>
                  </>
                );
              case 'offices-facilities':
                return (
                  <>
                  <OurFacilities />
                  </>
                );
                default:
                return null;
            }
          })()
          :
          <div className="w-full h-screen flex flex-col items-center bg-white">
                <div className="lg:w-2/3 lg:py-32 lg:mb-18">
                  <div className="text-4xl text-center"><h4>PAGE NOT FOUND</h4></div>
                </div>
              </div>
        }
                
            </>
        </main>
        </>
    )
}