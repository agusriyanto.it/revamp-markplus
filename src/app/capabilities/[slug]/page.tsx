import Link from "next/link"
import Image from 'next/image'
import parse from 'html-react-parser'
import { getNameCategori } from "@/app/libs/api"
import FindMoreDev from "@/app/components/findmoredev"
import parseHtml from "@/app/utils/parser"
import { AiOutlineArrowRight } from "react-icons/ai"

async function getData(params: string) {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: `
          query getCategory {
            category(id: "${params}", idType: SLUG) {
              posts {
                nodes {
                  title
                  content
                  slug
                  featuredImage {
                    node {
                      altText
                      sourceUrl
                      slug
                    }
                  }
                  seo {
                    metaDesc
                    title
                  }
                }
              }
            }
          }`
      })
    })
    const { data } = await res.json()
    return data.category.posts.nodes
  } catch (error) {
    <p>404: Page Not Found</p>
  }
}


export default async function Capabilities({ params }: { params: { slug: string } }) {

  interface Caty {
    seo: {
      metaDesc: string
      title: string
    };
    categories: {
      nodes: {
        name: string
      }[];
    };
    featuredImage: {
      node: {
        altText: string
        sourceUrl: string
        caption: string
      };
    };
    content: string
    title: string
    slug: string
  }

  interface Name {
    name: string
    description: string
  }

  const data: Caty[] = await getData(params.slug)
  const dt: Name = await getNameCategori(params.slug)
  return (
    <>
      <main className="flex-col sm:w-full sm:h-full mx-auto" >
        <div className="relative w-full h-auto" style={{ width: '100%', height: '50%', maxHeight: '80vh', overflow: 'hidden' }}>
          {(() => {
            switch (dt.name) {
              case 'Consumer Research':
                return (
                  <>
                    <div className='w-full relative min-h-[35rem]'>
                      <Image
                        src={'https://imagedelivery.net/2MtOYVTKaiU0CCt-BLmtWw/be44d378-f7a2-49a0-c2b2-518dca4c6a00/public'}
                        alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                      />

                    </div>

                  </>
                );
              case 'Advertising Effectiveness Study':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Brand Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Branding & Marketing Communication':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Customer Satisfaction Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}

                    />
                  </div>
                );
              case 'Diagnosis of issues':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Go to Market (Sales & Distribution)':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Market and Industry Analysis':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Marketing Audit':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Product & Pricing':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );

              case 'Product and Pricing Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Sales and Distribution Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Service Design & Customer Loyalty Strategy':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Social and Opinion Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Stakeholder Research':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              case 'Strategy':
                return (
                  <div className='w-full relative min-h-[35rem]'>
                    <Image
                      src={'/../../assets/images/article1.jpg'}
                      alt={'202'}
                        fill
                        className='w-full h-auto '
                        objectFit="cover"
                        priority
                        sizes="(min-width: 808px) 50vw, 100vw"
                        style={{
                          objectFit: 'cover',
                        }}
                    />
                  </div>
                );
              default:
                return null;
            }
          })()}
        </div>
        <div className="absolute max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
          <div className=' grid items-center' >
            <div className="items-center ">
              <h2 className='lg:text-5xl font-bold font-bannersm uppercase text-[#1B1072]'>{dt ? dt.name : ""}</h2>
            </div>
            {/* <div className="w-full mt-4">
              <h2 className="font-body lg:text-2xl ">{dt ? dt.description : ""}</h2>
            </div> */}
            {/* <div className="mt-4">
                  <Link href={CMS_URL+dt.slug} className='button bg-red-500 text-white text-sm py-2 px-4 rounded'> Read Now</Link>
                </div> */}
          </div>
        </div>

        <div className="w-full h-auto bg-white">
          <div className="max-w-[1440px] mx-auto w-full lg:h-auto lg:flex lg:flex-col bg-white lg:p-4">

            <>
              <div className="w-full flex flex-col items-center lg:px-16 bg-white">
                <div className="lg:w-full lg:mb-18 py-14 p-4">
                  <section>
                    <h2 className="items-center text-xl font-body text-slate-800" >{dt.description !== null ? parseHtml(dt.description) : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"}</h2>
                  </section>
                </div>
              </div>
            </>



            <div className="lg:w-full h-auto flex flex-col lg:px-20 gap-4 mb-10">
              {
                Array.isArray(data) && data.length > 0
                  ? data.map((items, i1) => (
                    <>
                      <div className="w-full h-auto justify-between lg:flex hover:shadow-lg hover:border-2" key={i1}>
                        <div className="w-full h-auto">
                          <Image
                            src={items.featuredImage !== null ? items.featuredImage.node.sourceUrl : "/../../assets/images/featured3.jpg"}
                            width={800}
                            height={500}
                            alt={items.featuredImage !== null ? items.featuredImage.node.altText : "Picture of the author"}
                            className="w-full h-auto bg-cover"
                          />
                        </div>
                        <div className="w-full h-auto p-8 gap-4">
                          <div className="w-full h-auto ">
                            <div className="text-4xl font-bold text-[#1B1072] mb-5 font-titlea">{items ? items.title : ""}</div>
                            <div className="text-xl font-body text-slate-800">
                              {items ? parse(items.seo.metaDesc.substring(0, 250)) : ""}...
                            </div>
                            <div className="text-2xl mt-4">
                              <Link href={"../" + items.slug} className="flex hover:translate-x-4 transition ease-in-out delay-500 duration-300 hover:text-blue-700">
                                <div className="text-[#1B1072] leading-none">Read More </div>
                                <AiOutlineArrowRight className="ml-2 " />
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  ))
                  :
                  <div className="w-full flex flex-col items-center bg-white">
                    <div className="lg:w-2/3 lg:py-10 lg:mb-18">
                      <div className="text-4xl text-center"><h4>NO DATA FOUND</h4></div>
                    </div>
                  </div>
              }
            </div>
          </div>
        </div>
      </main>
    </>
  )
}