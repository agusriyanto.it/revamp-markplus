import CardFeatured from '@/app/components/CardFeatured';
import CardFeaturedSlug from '@/app/components/CardFeaturedSlug';
import TopPage from '@/app/components/TopPage';
import { getIdCategoryBySlug, getNameCategori, getPostByCategoryId, getSlugHome } from '@/app/libs/api'
import parseHtml from '@/app/utils/parser';
import Image from 'next/image'
import Link from 'next/link'

interface Data {
    parent: {
        node: {
            name: string
        };
    };
    description: string
    slug: string
    name: string
    title: string
    posts: {
        nodes: post[]
        pageInfo: {
            endCursor: String
            hasNextPage: Boolean
            hasPreviousPage: Boolean
        }
    }
    children: {
        nodes: childrens[];
    }
}

interface post {
    title: string
    slug: string
    name: string
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    seo: {
        metaDesc: string
    }
}

interface childrens {
    id: number
    name: string
    title: string
    posts: {
        nodes: post[]
    }
}

interface Catid {
    categoryId: number
}

interface byCatId {
    nodes : postItem[]
    pageInfo: {
        endCursor: String
        hasNextPage: Boolean
        hasPreviousPage: Boolean
    }
}

interface postItem{
    title: String
    slug: String
    seo: {
        metaDesc: String
    }
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    categories: {
        nodes: categori[]
    }       
}
interface categori{
    name:string
}

export default async function Post({ params }: { params: { slug: string, after: string } }) {
    const data: Data = await getSlugHome(params.slug, params.after)
    const catId: Catid[] = await getIdCategoryBySlug(params.slug)
    const categoryIdStrings = catId.map(cat => cat.categoryId.toString())
    const numberArray = categoryIdStrings.map(str => parseInt(str))
    const postsbyid: byCatId = await getPostByCategoryId(numberArray, params.after)

    return (
        <>
            <link rel="canonical" href={`https://www.staging.markplusinc.com/${params.slug}/`} key="canonical" />
            <title>{data !== null ? data.name + " | MarkPlus.Inc" : params.slug + " | MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/${params.slug}/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />

            <main className="flex-col w-full mx-auto" >
                <TopPage />
                <div className="w-full flex flex-col justify-center lg:items-center bg-white">
                    <div className="h-auto lg:w-2/3 mt-32 bg-white p-4">
                        <div className="lg:text-6xl text-xl font-bold text-[#1B1072] font-titlea lg:w-2/4 uppercase">{data !== null ? data.name : ""} <span className='text-rose-700'>.</span></div>
                    </div>
                    {
                        data.name === "Ideas" ? <CardFeatured items={postsbyid} keyword={numberArray} /> : <CardFeaturedSlug items={data} keyword={params.slug} />
                    }
                    {/* <CardFeatured items={data} keyword={params.slug} /> */}
                    {/* {
                         postsbyid ? postsbyid.children !== null
                            ?
                                data.children.nodes.map((dt,i)=>(
                                    dt.posts.nodes.map((dp,ip)=>(
                                        <>
                                        <div className="w-full h-auto justify-between lg:flex mt-5" key={i}>
                                            <Link href={'/' + dp.slug} className="relative lg:flex lg:flex-col mt-6 text-gray-700 bg-white  hover:shadow-lg bg-clip-border w-96" >
                                                <div className="w-full h-auto" key={ip}>
                                                    <Image
                                                        src={dp.featuredImage !== null ? dp.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                        alt={dp.featuredImage !== null ? dp.featuredImage.node.altText : "Picture of the author"}
                                                        width={200}
                                                        height={112.5}
                                                        quality={90}
                                                        className='w-full h-[220px]'
                                                        sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                    />
                                                </div>
                                                <div className="w-full h-auto gap-4">
                                                    <div className="w-full h-auto ">
                                                        <div className="text-md text-slate-800  mt-4 font-title uppercase">{dt ? dt.name : ""} </div>
                                                        <div className="text-2xl font-titlea text-[#1B1072] uppercase">{dp ? dp.title : ""}</div>
                                                        <h2 className='text-xl font-body text-slate-800 mt-4'>{dp ? dp.seo.metaDesc.slice(0, 150)+ " ..." : ""}</h2>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                        </>
                                    ))
                                        
                                ))
                            : ""

                            : 
                            <div className="w-full h-auto justify-between place-items-center lg:flex mt-5">
                            <span className='text-2xl uppercase font-titles text-center'>Page Not Found</span>
                            </div>
                    }

                    {
                        data ? data.posts !== null
                            ?
                            data.posts.nodes.map((dt, index) => (
                                dt.seo.metaDesc !== "" ?
                                    <>
                                        <div className="w-full h-auto justify-between lg:flex mt-5">
                                            <Link href={'/' + dt.slug} className="relative lg:flex lg:flex-col mt-6 text-gray-700 bg-white hover:shadow-lg bg-clip-border w-96" key={index} >
                                                <div className="w-full h-auto" >
                                                    <Image
                                                        src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                        alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                                        width={200}
                                                        height={112.5}
                                                        quality={90}
                                                        className='w-full h-[220px]'
                                                        sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                                    />
                                                </div>
                                                <div className="w-full h-auto gap-4">
                                                    <div className="w-full h-auto ">
                                                        <div className="text-md text-slate-800  mt-4 font-title uppercase">{dt ? dt.name : ""}</div>
                                                        <div className="text-2xl font-titlea text-[#1B1072] uppercase">{dt ? dt.title : ""}</div>
                                                        <h2 className='text-xl font-body text-slate-800 mt-4'>{dt ? dt.seo.metaDesc.slice(0, 150)+ " ..." : ""}</h2>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                    </>
                                    : ""

                            ))

                            : ""
                            : ""
                    } */}
                </div>
            </main>
        </>
    )
}