import Link from "next/link"

import Industries from "../components/home/industries"
import GetAllChildrens from "../components/featuredinsights/getAllChildrens"

async function getData(params: string) {
  const caty = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: `
        query getSingelPage {
            posts(where: {name: "${params}"}) {
                nodes {
                  slug
                  content(format: RENDERED)
                  title(format: RENDERED)
                }
              }
        }`
    })
  })
  const { data } = await caty.json()
  return data.posts.nodes
}

export default async function Post({ params }: { params: { slug: string, title: string, content: string, name: string } }) {
  const data = await getData(params.slug)
  return (
    <>
      <link rel="canonical" href={`https://www.staging.markplusinc.com/${params.slug}/`} key="canonical" />
            <title>{params.slug + " | MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta name="keywords" content={data ? data.title : ""} />
            <meta name="news_keywords" content={data ? data.title : ""} />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:title" content={data ? data.title : ""} />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/${params.slug}/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <meta name="twitter:title" content={data ? data.title : "MarkPlus.Inc"} />
            <meta name="content_tags" content={data ? data.title : "MarkPlus.Inc"} />
      <main className="lg:flex lg:flex-col items-center justify-between" >
        <div className="text-white lg:flex bg-no-repeat bg-cover w-full bg-white h-auto">
          <div className="container grid items-start overflow-hidden lg:px-24 lg:w-4/5 p-4">
            <div className="items-center lg:w-1/3">
              <p className='mt-20 text-6xl mb-4 sm:font-medium uppercase fontsectioncap text-[#1B1072]'>Featured <br /> Insights<span className="text-red-600">.</span></p>
            </div>
          </div>
        </div>
        <GetAllChildrens/>
      </main>
    </>
  )
}