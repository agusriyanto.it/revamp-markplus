import { getParentBySlug, getPostsByIdTags, getSinglePg, getTagsBySlug } from '@/app/libs/api'
import { AiFillCaretRight } from 'react-icons/ai'
import parseHtml from '../utils/parser'
import Image from 'next/image'
import { format } from 'date-fns'
import CardTags from '../components/CardTags'
import TopPage from '../components/TopPage'
import HeaderSlug from '../components/HeaderSlug'

interface categori {
    name: string
    slug: string
    description: string
    parent: {
        node: {
            name: string
        }
    }
}

interface Data {
    categories: {
        nodes: {
            categori: categori[]
        }
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
            caption: string
        };
    }
    seo: {
        metaDesc: string
    }
    content: string
    slug: string
    title: string
    date: Date
}

interface Cy {
    parent: {
        node: {
            name: string
        };
    };
    description: string
    slug: string
    name: string
}

interface Tagid {
    name: string
    databaseId: number
}

interface byTagId {
    nodes: postItem[]
    pageInfo: {
        endCursor: String
        hasNextPage: Boolean
        hasPreviousPage: Boolean
    }
}

interface postItem {
    title: String
    slug: String
    seo: {
        metaDesc: String
    }
    featuredImage: {
        node: {
            altText: string
            title: string
            sourceUrl: string
        }
    }
    categories: {
        nodes: categori[]
    }
}
interface categori {
    parent: {
        node: {
            name: string
        }
    }
}

export default async function Post({ params }: { params: { slug: string, after: string } }) {
    const data: Data = await getSinglePg(params.slug)
    const dt: Cy[] = await getParentBySlug(params.slug)
    const tagsId: Tagid[] = await getTagsBySlug(params.slug)
    const tagIdStrings = tagsId.map(cat => cat.databaseId.toString())
    const numberArray = tagIdStrings.map(str => parseInt(str))
    const postsbyid: byTagId = await getPostsByIdTags(numberArray, params.after)
    return (
        <>
            <link rel="canonical" href={`https://www.staging.markplusinc.com/${params.slug}/`} key="canonical" />
            <title>{data ? data.title + " | MarkPlus.Inc" : "MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta name="keywords" content={data ? data.title : ""} />
            <meta name="news_keywords" content={data ? data.title : ""} />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : "/../../assets/images/meta-img.jpg"} />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:title" content={data ? data.title : ""} />
            <meta property="og:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/${params.slug}/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <meta name="twitter:title" content={data ? data.title : "MarkPlus.Inc"} />
            <meta name="twitter:description" content={data ? data.seo !== null ? data.seo.metaDesc : "" : "MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta name="twitter:image" content={data ? data.featuredImage === null ? "/../../assets/images/meta-img.jpg" : data.featuredImage.node.sourceUrl : "/../../assets/images/meta-img.jpg"} />
            <meta name="content_tags" content={data ? data.title : "MarkPlus.Inc"} />
            <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{ __html: JSON.stringify(data ? data.title : "MarkPlus.Inc") }}
            />
            <main className="flex-col sm:w-full sm:h-full mx-auto" >
                <TopPage />
                {
                    data !== null
                        ?
                        <HeaderSlug items={data} data={dt} />
                        : <div className="bg-white lg:flex bg-no-repeat bg-cover w-full lg:h-screen h-72 items-center justify-center">
                            <div className='py-28 ' >
                                <div className="overflow-hidden lg:px-24 p-6 items-center justify-between">
                                    <div className="items-center">
                                        <span className='font-title text-black flex gap-4 text-4xl font-bold uppercase'>No Data Found</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
                <div className="w-full flex flex-col justify-center items-center lg:px-24 bg-white">
                    <div className="max-w-5xl mx-auto lg:my-24 lg:mb-18 lg:px-2 mt-8 px-6">
                        <span className='font-title lg:text-md text-[14px] uppercase text-[#1B1072]'>{dt ? dt.map((dt, i) => (<><span key={i}>{dt.parent.node.name}</span></>)) : ""} - {data ? format(new Date(data.date), 'MMM dd, yyyy') : ""}</span>
                        <div className="lg:text-4xl text-2xl font-bold text-[#1B1072] mb-10 font-titlea uppercase">{data ? data.title : ""} </div>
                        <section>
                            <div className="items-center text-2xl font-body text-slate-800" >{data ? parseHtml(data.content) : ""}</div>
                        </section>
                        {
                            postsbyid
                                ? postsbyid.nodes.length > 1
                                    ?
                                    <>
                                        <div className="max-w-5xl mx-auto mt-16">
                                            <div className="w-full border border-solid border-blue-950 opacity-50"></div>
                                            <h4 className='font-title w-fit h-12 p-3 flex text-white bg-blue-950 lg:text-lg text-sm uppercase'><AiFillCaretRight className='text-red-500' />Related Articles</h4>
                                            <div className='flex flex-col justify-center items-center'>
                                                <CardTags items={postsbyid} keyword={numberArray} slug={params.slug} />
                                            </div>
                                        </div>
                                    </>
                                    : ""
                                : ""
                        }
                    </div>
                </div>
            </main >
        </>
    )
}