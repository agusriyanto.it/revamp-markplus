import Link from 'next/link'
import React from 'react'
import { getIndustries } from '../libs/api'
import Image from 'next/image'

interface Caty {
    title: string
    slug: string
    seo: {
        metaDesc: string
    }
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
        }
    }
    categories: {
        nodes: categori[]

    }
}

interface categori {
    parent: {
        node: {
            name: string
        }
    }
}

const CardIndustries = async () => {
    const Ideas: Caty[] = await getIndustries(3)
    return (
        <section className="w-full h-auto">
            {
                Array.isArray(Ideas) && Ideas.length > 0
                    ? Ideas.map((dt, i) => (
                        <>
                            <div className="w-full lg:flex justify-between text-dark lg:mt-20 mt-8 bg-white" key={i}>
                                <div className={`w-full lg:min-h-[100%] ${i % 2 === 0 ? 'lg:order-first lg:-translate-x-40' : 'lg:order-last lg:transla'}`}>
                                    <Image
                                        src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                        alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                        width={200}
                                        height={112.5}
                                        quality={90}
                                        className='w-full h-auto'
                                        sizes='(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw'
                                    />
                                </div>
                                <div className='w-full items-center justify-between bg-white p-4'>
                                    <h2 className='lg:text-[44px] text-xl font-bold uppercase font-title text-[#1B1072]'>{dt ? dt.title : ""}<span className='text-red-600'>.</span></h2>
                                    <p className='text-slate-800 lg:text-2xl text-base font-body lg:mt-8'>{dt ? dt.seo.metaDesc : ""}</p>
                                </div>
                            </div>

                        </>
                    ))
                    : ""
            }

            {/* <div className="h-auto lg:w-full lg:grid lg:grid-cols-4 lg:px-24 bg-white gap-2 p-4">
                {
                    Array.isArray(Ideas) && Ideas.length > 0
                        ? Ideas.map((dt, i) => (
                            <>
                                <div className="w-full lg:flex items-center justify-between text-dark lg:mt-20 bg-white" key={i}>
                                    <div className='w-full lg:-translate-x-36 '>
                                        <Image
                                            src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                            alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                            width={200}
                                            height={112.5}
                                            quality={90}
                                            className='w-full'
                                            sizes='(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw'
                                        />
                                    </div>
                                    <div className='w-full items-center justify-between bg-white'>
                                        <div className="p-4">
                                            <h2 className='text-2xl font-bold uppercase font-title text-[#1B1072]'>{dt ? dt.title : ""}<span className='text-red-600'>.</span></h2>
                                            <p className='text-slate-800 lg:text-2xl text-xl font-body mt-8'>{dt ? dt.seo.metaDesc.substring(0, 110) + " ..." : ""}
                                            </p>
                                        </div>
                                    </div>
                                </div>




                                <div className="w-full h-auto justify-between lg:flex lg:mt-14 mt-10" key={i}>
                                    <Link href={dt.slug} className="lg:flex lg:flex-col text-gray-700 bg-white  bg-clip-border w-96">
                                        <div className="w-full h-auto" key={i} >
                                            <Image
                                                src={dt.featuredImage !== null ? dt.featuredImage.node.sourceUrl : "/../../assets/images/img-default.jpg"}
                                                alt={dt.featuredImage !== null ? dt.featuredImage.node.altText : "Picture of the author"}
                                                width={200}
                                                height={112.5}
                                                quality={90}
                                                className='w-full h-[220px]'
                                                sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
                                            />
                                        </div>
                                        <div className="w-full h-auto ">
                                            <div className="lg:text-md text-[14px] text-slate-800  lg:mt-4 mt-2 font-title uppercase">
                                                {dt.categories.nodes.map((dt2, i) => (
                                                    <>
                                                        {dt2.parent.node.name}<span className='text-rose-700'>.</span>
                                                    </>
                                                ))}
                                            </div>
                                            <div className="lg:text-2xl text-xl hover:underline font-titlea text-[#1B1072] uppercase">{dt ? dt.title : ""}</div>
                                            <h2 className='lg:text-xl text-[14px] font-body text-slate-800 lg:mt-4 mt-2'>{dt ? dt.seo.metaDesc.substring(0, 110) + " ..." : ""}</h2>
                                        </div>
                                    </Link>
                                </div>
                            </>
                        ))
                        : ""
                }
            </div> */}
        </section >
    )
}

export default CardIndustries