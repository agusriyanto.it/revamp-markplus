import React from 'react'
import TopPage from '../components/TopPage'
import CardIndustries from './cardIndustries'

const page = () => {
    return (
        <>
            <link rel="canonical" href={`https://www.staging.markplusinc.com/`} key="canonical" />
            <title>{"Featured Insights | MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:locale" content="id_ID" />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <main className="lg:flex lg:flex-col bg-white" >
                <div className="lg:w-1/3 lg:px-24 p-4">
                    <h4 className='mt-32 lg:text-6xl text-md uppercase fontsectioncap text-[#1B1072]'>Industries<span className="text-red-600">.</span></h4>
                </div>
                <TopPage />
                <CardIndustries />
            </main>
        </>
    )
}

export default page