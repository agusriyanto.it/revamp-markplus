import Image from 'next/image'
import Link from 'next/link'
import { FaEnvelope, FaMapMarkerAlt, FaPhone, FaRegClock } from 'react-icons/fa'
import TopPage from '../components/TopPage'

export default function Contacts() {
    return (
        <>
            <link rel="canonical" href={`https://www.staging.markplusinc.com/contacts/`} key="canonical" />
            <title>{"Contact Us | MarkPlus.Inc" }</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta name="keywords" content={'Contact Us'} />
            <meta name="news_keywords" content={'Contact Us'} />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:image" content={`url(/../../assets/images/meta-img.jpg)`} />
            <meta property="og:locale" content="id_ID" />
            <meta property="og:title" content={'Contact Us'} />
            <meta property="og:description" content={"MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta property="og:url" content={`https://www.staging.markplusinc.com/contacts/`} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <meta name="twitter:title" content={'Contact Us'} />
            <meta name="twitter:description" content={"MarkPlus Inc. operates through four distinct business divisions: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, and MarkPlus Islamic. MarkPlus Consulting serves as the premier consulting arm of our organization, dedicated to delivering innovative solutions and strategic guidance across various marketing challenges. MarkPlus Insight specializes in comprehensive marketing and social research, delivering actionable insights and reliable data to drive informed decision-making. MarkPlus Tourism and MarkPlus Islamic divisions were established to cater to the specific needs of the rapidly expanding tourism and Islamic economy sectors, offering tailored research and consultancy services to meet the diverse demands of these industries"} />
            <meta name="twitter:image" content={`url(/../../assets/images/meta-img.jpg)`} />
            <meta name="content_tags" content={"Contact Us | MarkPlus.Inc"} />
            <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{ __html: JSON.stringify("Contact Us | MarkPlus.Inc") }}
            />
            <TopPage />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
            <div className=" max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
                <div className='w-full items-center lg:py-40 py-20 h-auto justify-between p-4 bg-white'>
                    <div className='lg:px-20 w-full'>
                        <h2 className='text-6xl uppercase font-banner text-[#1B1072] mt-16'>Contact Us<span className="text-red-600">.</span></h2>
                    </div>
                    <div className='lg:px-20 w-full lg:mt-28 mt-8'>
                        <h2 className='lg:text-4xl text-xl font-title uppercase text-black font-bold'>Get in Touch.</h2>
                        <h2 className='lg:text-2xl text-lg font-body text-black lg:mt-4 mt-2'>We are built to serve and established to build. Since founded in 1990 by Hermawan Kartajaya, we have consistently aimed to develop companies and individuals in the spheres we master best: consulting, marketing research, education, and media and community.</h2>
                    </div>
                    <div className='lg:px-20 w-full lg:mt-20 mt-10'>
                        <h2 className='lg:text-4xl text-xl font-title uppercase text-black font-bold'>Our Office.</h2>
                        <h2 className='lg:text-2xl font-body text-black lg:mt-4 mt-2'>
                            <div className='w-full items-center bg-white'>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaMapMarkerAlt /> Address: EightyEight@Kasablanka, 8th Floor</span>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaPhone /> Phone: +62 21 5790 2338</span>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaEnvelope /> Email: <Link href='mailto:info@markplusinc.com' className='text-blue-700 font-bold'>info@markplusinc.com</Link></span>
                            </div>
                        </h2>
                    </div>
                    <div className='lg:px-20 w-full lg:mt-20 mt-10'>
                        <h2 className='ld:text-4xl text-xl font-title uppercase text-black font-bold'>Business Hours.</h2>
                        <h2 className='lg:text-2xl text-lg font-body lg:mt-4 mt-2'>
                            <div className='w-full items-center bg-white'>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaRegClock /> Monday - Friday - 8am to 5pm</span>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaRegClock /> Saturday - 9am to 2pm</span>
                                <span className='font-body lg:text-2xl text-lg flex items-center text-black gap-4 lg:mt-4 mt-2'><FaRegClock />Sunday - Closed</span>
                            </div>
                        </h2>
                    </div>
                </div>
            </div>
        </main>
        </>
        
    )
}
