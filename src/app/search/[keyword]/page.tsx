import TopPage from '@/app/components/TopPage'
import { getSearch } from '../../libs/api'
import Card from '@/app/components/Card'

interface Data{
    nodes :Caty[]
    pageInfo : {
        endCursor : String
        hasNextPage : Boolean
        hasPreviousPage : Boolean
    }
}
interface Caty {
    title: string
    slug: string
    date: Date
    featuredImage: {
        node: {
            altText: string
            sourceUrl: string
        }
    }
    seo: {
        metaDesc: string
    }
    categories: {
        nodes: categ[]
    }   
}

interface categ {
    parent: {
        node: {
            name: string
        }
    }
}


export default async function Result({ params }: { params: { keyword: string,after:string } }) {
    const { keyword } = params
    const decodedKeyword = keyword.replace(/%20/g, ' ')
    const data: Data = await getSearch(decodedKeyword, params.after)
    return (
        <>
            <title>{keyword + " | MarkPlus.Inc"}</title>
            <meta name="googlebot-news" content="index,follow" />
            <meta name="googlebot" content="index,follow" />
            <meta name="robots" content="index, follow" />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="www.markplusinc.com" />
            <meta property="og:creator" content="markplusinc" />
            <meta property="og:locale" content="id_ID" />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@the_marketeers" />
            <meta name="twitter:creator" content="@the_marketeers" />
            <main className="lg:flex lg:flex-col items-center justify-between bg-white" >
                <>
                <TopPage />
                <div className=" max-w-[1440px] mx-auto w-full lg:top-36 top-20 lg:left-24 left-6">
                    <div className='w-full items-center lg:py-40 py-32 h-auto justify-between p-4 bg-white'>
                        <div className='lg:px-20 w-full'>
                            <h2 className='lg:text-6xl text-[20px] uppercase font-banner text-[#1B1072]'>Search Articles<span className="text-red-600">.</span></h2>
                            <h2 className='text-title mt-4 font-bold uppercase text-black'>Result Search : {decodedKeyword}</h2>
                        </div>                        
                        <Card items={data} keyword={decodedKeyword}/>
                    </div>
                </div>
                </>
                
            </main>
        </>
    )
}