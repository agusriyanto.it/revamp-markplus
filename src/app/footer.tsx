import Link from "next/link"
import HandleSub from "./components/hanleSubscibe";
import Image from 'next/image'


export default function Footer() {

    return (
        <section className="flex-col items-center sm:w-full sm:h-full">
            <footer className="bg-stone-100 py-6 lg:px-20">
                <div className="lg:w-1/4 h-auto p-4">
                    <Link href="/">
                        <Image
                            src="/../../assets/images/logorev.png"
                            width={130}
                            height={100}
                            alt="Markplus Indonesia"
                            className="w-auto lg:h-[45px] h-[30px]"
                        />
                    </Link>
                </div>
                <div className="w-full h-auto lg:flex p-4 items-center justify-between">
                    <div className="mb-8 lg:w-2/4">
                        <div className="text-slate-800 lg:text-xl mb-2 font-body ">MarkPlus Inc. beroperasi melalui empat divisi bisnis yang berbeda: MarkPlus Consulting, MarkPlus Insight, MarkPlus Tourism, dan MarkPlus Islamic. MarkPlus Consulting berfungsi sebagai cabang konsultasi utama di organisasi yang berdedikasi untuk memberikan solusi inovatif dan panduan strategis dalam berbagai tantangan pemasaran. MarkPlus Insight berspesialisasi dalam penelitian pemasaran dan sosial yang komprehensif, memberikan wawasan yang dapat ditindaklanjuti dan data yang andal untuk mendorong pengambilan keputusan yang tepat. Divisi MarkPlus Tourism dan MarkPlus Islamic didirikan untuk memenuhi kebutuhan spesifik sektor pariwisata dan ekonomi Islam yang berkembang pesat, menawarkan layanan penelitian dan konsultasi yang disesuaikan untuk memenuhi beragam permintaan industri ini.</div>
                    </div>
                    <div className="flex w-auto flex-col lg:text-right">
                        <div className="w-full h-auto flex gap-4 items-center justify-end">
                            <HandleSub />
                        </div>
                        <div className="w-full h-auto mt-4 lg:flex lg:gap-4 items-center justify-end">
                            <div>
                                <Link href="/privacy-policy" className="mt-4 lg:inline-block font-primary"> Privacy Policy</Link>
                            </div>
                            <div>
                                <Link href="/" className="mt-4 lg:inline-block font-primary"> Term & Conditions</Link>
                            </div>
                            <div>
                                <Link href="/contacts" className="mt-4 lg:inline-block font-primary"> Contact Us</Link>
                            </div>
                            <div>
                                <Link href="/" className="mt-4 lg:inline-block font-primary ">Responsible Disclosure</Link>
                            </div>
                        </div>
                        <div className="w-full h-auto mt-4 flex lg:gap-4 items-center lg:justify-end">
                            {/* <h3 className="text-4md ">Follow us</h3> */}
                            <div className="mt-0 lg:ml-2 lg:text-4xl text-2xl lg:gap-2  justify-center items-center flex">
                                <Link href='https://id.linkedin.com/company/markplus-inc' target='__blank'>
                                    <Image
                                        src={'/../../assets/images/sm/Linkedin.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    />
                                </Link>
                                <Link href='https://www.facebook.com/markplusinc/' target='__blank'>
                                    <Image
                                        src={'/../../assets/images/sm/FB.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    /></Link>
                                <Link href='https://twitter.com/MarkPlus_Inc' target='__blank'>
                                    <Image
                                        src={'/../../assets/images/sm/X.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    />
                                </Link>
                                <Link href='https://www.instagram.com/markpluscorp' target='__blank'>
                                    <Image
                                        src={'/../../assets/images/sm/IG.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    />
                                </Link>
                                <Link href='https://www.youtube.com/@markpluschannel1932' target="__blank">
                                    <Image
                                        src={'/../../assets/images/sm/YouTube.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    />
                                </Link>
                                <Link href='https://www.tiktok.com/@markpluscorp' target='__blank'>
                                    <Image
                                        src={'/../../assets/images/sm/TikTok.png'}
                                        alt={"Picture of the author"}
                                        width={40}
                                        height={40}
                                        quality={100}
                                        className='lg:w-full w-2/3 lg:h-auto'
                                    />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div className="max-w-sm w-full lg:max-w-full lg:flex">
                <div className="bg-white w-full p-4 lg:flex lg:flex-col justify-between lg:px-24">
                    <div className="mb-8">
                        <div className="text-gray-900 w-full bg-white lg:flex lg:grid-cols-12 font-primary lg:text-xl">
                            <span className="lg:text-xl">&copy; 2024 MarkPlus.Inc - All Rights Reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

